<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>3</int>
        <key>texturePackerVersion</key>
        <string>4.1.0</string>
        <key>fileName</key>
        <string>/Volumes/CLALASD/art/shooter/pixel-item.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>premultiplyAlpha</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>corona-imagesheet</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>2</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>reduceBorderArtifacts</key>
        <false/>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>forceWordAligned</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>lua</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Project/shooting/sprites/pixeleffect.lua</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>cleanTransparentPixels</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../pixel-effect/1/1.png</key>
            <key type="filename">../pixel-effect/1/2.png</key>
            <key type="filename">../pixel-effect/1/3.png</key>
            <key type="filename">../pixel-effect/1/4.png</key>
            <key type="filename">../pixel-effect/1/5.png</key>
            <key type="filename">../pixel-effect/1/6.png</key>
            <key type="filename">../pixel-effect/10/1.png</key>
            <key type="filename">../pixel-effect/10/2.png</key>
            <key type="filename">../pixel-effect/10/3.png</key>
            <key type="filename">../pixel-effect/10/4.png</key>
            <key type="filename">../pixel-effect/10/5.png</key>
            <key type="filename">../pixel-effect/10/6.png</key>
            <key type="filename">../pixel-effect/11/1.png</key>
            <key type="filename">../pixel-effect/11/2.png</key>
            <key type="filename">../pixel-effect/11/3.png</key>
            <key type="filename">../pixel-effect/11/4.png</key>
            <key type="filename">../pixel-effect/11/5.png</key>
            <key type="filename">../pixel-effect/11/6.png</key>
            <key type="filename">../pixel-effect/12/1.png</key>
            <key type="filename">../pixel-effect/12/2.png</key>
            <key type="filename">../pixel-effect/12/3.png</key>
            <key type="filename">../pixel-effect/12/4.png</key>
            <key type="filename">../pixel-effect/12/5.png</key>
            <key type="filename">../pixel-effect/12/6.png</key>
            <key type="filename">../pixel-effect/13/1.png</key>
            <key type="filename">../pixel-effect/13/2.png</key>
            <key type="filename">../pixel-effect/13/3.png</key>
            <key type="filename">../pixel-effect/13/4.png</key>
            <key type="filename">../pixel-effect/13/5.png</key>
            <key type="filename">../pixel-effect/13/6.png</key>
            <key type="filename">../pixel-effect/13/7.png</key>
            <key type="filename">../pixel-effect/14/1.png</key>
            <key type="filename">../pixel-effect/14/2.png</key>
            <key type="filename">../pixel-effect/14/3.png</key>
            <key type="filename">../pixel-effect/14/4.png</key>
            <key type="filename">../pixel-effect/14/5.png</key>
            <key type="filename">../pixel-effect/14/6.png</key>
            <key type="filename">../pixel-effect/14/7.png</key>
            <key type="filename">../pixel-effect/15/1.png</key>
            <key type="filename">../pixel-effect/15/11.png</key>
            <key type="filename">../pixel-effect/15/2.png</key>
            <key type="filename">../pixel-effect/15/3.png</key>
            <key type="filename">../pixel-effect/15/4.png</key>
            <key type="filename">../pixel-effect/15/5.png</key>
            <key type="filename">../pixel-effect/15/6.png</key>
            <key type="filename">../pixel-effect/15/7.png</key>
            <key type="filename">../pixel-effect/15/8.png</key>
            <key type="filename">../pixel-effect/15/9.png</key>
            <key type="filename">../pixel-effect/16/1.png</key>
            <key type="filename">../pixel-effect/16/11.png</key>
            <key type="filename">../pixel-effect/16/2.png</key>
            <key type="filename">../pixel-effect/16/3.png</key>
            <key type="filename">../pixel-effect/16/4.png</key>
            <key type="filename">../pixel-effect/16/5.png</key>
            <key type="filename">../pixel-effect/16/6.png</key>
            <key type="filename">../pixel-effect/16/7.png</key>
            <key type="filename">../pixel-effect/16/8.png</key>
            <key type="filename">../pixel-effect/16/9.png</key>
            <key type="filename">../pixel-effect/17/1.png</key>
            <key type="filename">../pixel-effect/17/2.png</key>
            <key type="filename">../pixel-effect/17/3.png</key>
            <key type="filename">../pixel-effect/18/1.png</key>
            <key type="filename">../pixel-effect/18/2.png</key>
            <key type="filename">../pixel-effect/18/3.png</key>
            <key type="filename">../pixel-effect/18/4.png</key>
            <key type="filename">../pixel-effect/18/5.png</key>
            <key type="filename">../pixel-effect/19/1.png</key>
            <key type="filename">../pixel-effect/19/2.png</key>
            <key type="filename">../pixel-effect/19/3.png</key>
            <key type="filename">../pixel-effect/19/4.png</key>
            <key type="filename">../pixel-effect/2/1.png</key>
            <key type="filename">../pixel-effect/2/2.png</key>
            <key type="filename">../pixel-effect/2/3.png</key>
            <key type="filename">../pixel-effect/2/4.png</key>
            <key type="filename">../pixel-effect/2/5.png</key>
            <key type="filename">../pixel-effect/2/6.png</key>
            <key type="filename">../pixel-effect/20/1.png</key>
            <key type="filename">../pixel-effect/20/2.png</key>
            <key type="filename">../pixel-effect/20/3.png</key>
            <key type="filename">../pixel-effect/20/4.png</key>
            <key type="filename">../pixel-effect/21/1.png</key>
            <key type="filename">../pixel-effect/21/2.png</key>
            <key type="filename">../pixel-effect/21/3.png</key>
            <key type="filename">../pixel-effect/21/4.png</key>
            <key type="filename">../pixel-effect/21/5.png</key>
            <key type="filename">../pixel-effect/21/6.png</key>
            <key type="filename">../pixel-effect/22/1.png</key>
            <key type="filename">../pixel-effect/22/2.png</key>
            <key type="filename">../pixel-effect/22/3.png</key>
            <key type="filename">../pixel-effect/22/4.png</key>
            <key type="filename">../pixel-effect/22/5.png</key>
            <key type="filename">../pixel-effect/23/1.png</key>
            <key type="filename">../pixel-effect/23/2.png</key>
            <key type="filename">../pixel-effect/23/3.png</key>
            <key type="filename">../pixel-effect/23/4.png</key>
            <key type="filename">../pixel-effect/23/5.png</key>
            <key type="filename">../pixel-effect/23/6.png</key>
            <key type="filename">../pixel-effect/24/1.png</key>
            <key type="filename">../pixel-effect/24/2.png</key>
            <key type="filename">../pixel-effect/24/3.png</key>
            <key type="filename">../pixel-effect/24/4.png</key>
            <key type="filename">../pixel-effect/24/5.png</key>
            <key type="filename">../pixel-effect/25/1.png</key>
            <key type="filename">../pixel-effect/25/2.png</key>
            <key type="filename">../pixel-effect/25/3.png</key>
            <key type="filename">../pixel-effect/25/4.png</key>
            <key type="filename">../pixel-effect/25/5.png</key>
            <key type="filename">../pixel-effect/26/1.png</key>
            <key type="filename">../pixel-effect/26/2.png</key>
            <key type="filename">../pixel-effect/26/3.png</key>
            <key type="filename">../pixel-effect/26/4.png</key>
            <key type="filename">../pixel-effect/26/5.png</key>
            <key type="filename">../pixel-effect/27/1.png</key>
            <key type="filename">../pixel-effect/27/2.png</key>
            <key type="filename">../pixel-effect/27/3.png</key>
            <key type="filename">../pixel-effect/27/4.png</key>
            <key type="filename">../pixel-effect/27/5.png</key>
            <key type="filename">../pixel-effect/27/6.png</key>
            <key type="filename">../pixel-effect/28/1.png</key>
            <key type="filename">../pixel-effect/28/2.png</key>
            <key type="filename">../pixel-effect/28/3.png</key>
            <key type="filename">../pixel-effect/28/4.png</key>
            <key type="filename">../pixel-effect/28/5.png</key>
            <key type="filename">../pixel-effect/28/6.png</key>
            <key type="filename">../pixel-effect/29/1.png</key>
            <key type="filename">../pixel-effect/29/2.png</key>
            <key type="filename">../pixel-effect/29/3.png</key>
            <key type="filename">../pixel-effect/29/4.png</key>
            <key type="filename">../pixel-effect/29/5.png</key>
            <key type="filename">../pixel-effect/29/6.png</key>
            <key type="filename">../pixel-effect/3/1.png</key>
            <key type="filename">../pixel-effect/3/2.png</key>
            <key type="filename">../pixel-effect/3/3.png</key>
            <key type="filename">../pixel-effect/3/4.png</key>
            <key type="filename">../pixel-effect/3/5.png</key>
            <key type="filename">../pixel-effect/3/6.png</key>
            <key type="filename">../pixel-effect/30/1.png</key>
            <key type="filename">../pixel-effect/30/2.png</key>
            <key type="filename">../pixel-effect/30/3.png</key>
            <key type="filename">../pixel-effect/30/4.png</key>
            <key type="filename">../pixel-effect/30/5.png</key>
            <key type="filename">../pixel-effect/30/6.png</key>
            <key type="filename">../pixel-effect/31/1.png</key>
            <key type="filename">../pixel-effect/31/2.png</key>
            <key type="filename">../pixel-effect/31/3.png</key>
            <key type="filename">../pixel-effect/31/4.png</key>
            <key type="filename">../pixel-effect/31/5.png</key>
            <key type="filename">../pixel-effect/32/1.png</key>
            <key type="filename">../pixel-effect/32/2.png</key>
            <key type="filename">../pixel-effect/32/3.png</key>
            <key type="filename">../pixel-effect/32/4.png</key>
            <key type="filename">../pixel-effect/32/5.png</key>
            <key type="filename">../pixel-effect/32/6.png</key>
            <key type="filename">../pixel-effect/32/7.png</key>
            <key type="filename">../pixel-effect/32/8.png</key>
            <key type="filename">../pixel-effect/33/1.png</key>
            <key type="filename">../pixel-effect/33/2.png</key>
            <key type="filename">../pixel-effect/33/3.png</key>
            <key type="filename">../pixel-effect/33/4.png</key>
            <key type="filename">../pixel-effect/34/1.png</key>
            <key type="filename">../pixel-effect/34/2.png</key>
            <key type="filename">../pixel-effect/34/3.png</key>
            <key type="filename">../pixel-effect/34/4.png</key>
            <key type="filename">../pixel-effect/34/5.png</key>
            <key type="filename">../pixel-effect/34/6.png</key>
            <key type="filename">../pixel-effect/34/7.png</key>
            <key type="filename">../pixel-effect/34/8.png</key>
            <key type="filename">../pixel-effect/34/9.png</key>
            <key type="filename">../pixel-effect/35/1.png</key>
            <key type="filename">../pixel-effect/35/2.png</key>
            <key type="filename">../pixel-effect/35/3.png</key>
            <key type="filename">../pixel-effect/35/4.png</key>
            <key type="filename">../pixel-effect/35/5.png</key>
            <key type="filename">../pixel-effect/35/6.png</key>
            <key type="filename">../pixel-effect/36/1.png</key>
            <key type="filename">../pixel-effect/36/2.png</key>
            <key type="filename">../pixel-effect/36/3.png</key>
            <key type="filename">../pixel-effect/36/4.png</key>
            <key type="filename">../pixel-effect/36/5.png</key>
            <key type="filename">../pixel-effect/36/6.png</key>
            <key type="filename">../pixel-effect/37/1.png</key>
            <key type="filename">../pixel-effect/37/2.png</key>
            <key type="filename">../pixel-effect/37/3.png</key>
            <key type="filename">../pixel-effect/37/4.png</key>
            <key type="filename">../pixel-effect/37/5.png</key>
            <key type="filename">../pixel-effect/37/6.png</key>
            <key type="filename">../pixel-effect/38/1.png</key>
            <key type="filename">../pixel-effect/38/2.png</key>
            <key type="filename">../pixel-effect/38/3.png</key>
            <key type="filename">../pixel-effect/38/4.png</key>
            <key type="filename">../pixel-effect/38/5.png</key>
            <key type="filename">../pixel-effect/38/6.png</key>
            <key type="filename">../pixel-effect/39/1.png</key>
            <key type="filename">../pixel-effect/39/2.png</key>
            <key type="filename">../pixel-effect/39/3.png</key>
            <key type="filename">../pixel-effect/39/4.png</key>
            <key type="filename">../pixel-effect/39/5.png</key>
            <key type="filename">../pixel-effect/39/6.png</key>
            <key type="filename">../pixel-effect/39/7.png</key>
            <key type="filename">../pixel-effect/4/1.png</key>
            <key type="filename">../pixel-effect/4/2.png</key>
            <key type="filename">../pixel-effect/4/3.png</key>
            <key type="filename">../pixel-effect/4/4.png</key>
            <key type="filename">../pixel-effect/4/5.png</key>
            <key type="filename">../pixel-effect/4/6.png</key>
            <key type="filename">../pixel-effect/40/1.png</key>
            <key type="filename">../pixel-effect/40/2.png</key>
            <key type="filename">../pixel-effect/40/3.png</key>
            <key type="filename">../pixel-effect/41/1.png</key>
            <key type="filename">../pixel-effect/41/2.png</key>
            <key type="filename">../pixel-effect/41/3.png</key>
            <key type="filename">../pixel-effect/42/1.png</key>
            <key type="filename">../pixel-effect/42/2.png</key>
            <key type="filename">../pixel-effect/42/3.png</key>
            <key type="filename">../pixel-effect/43/1.png</key>
            <key type="filename">../pixel-effect/43/2.png</key>
            <key type="filename">../pixel-effect/43/3.png</key>
            <key type="filename">../pixel-effect/44/1.png</key>
            <key type="filename">../pixel-effect/44/2.png</key>
            <key type="filename">../pixel-effect/44/3.png</key>
            <key type="filename">../pixel-effect/45/1.png</key>
            <key type="filename">../pixel-effect/45/2.png</key>
            <key type="filename">../pixel-effect/45/3.png</key>
            <key type="filename">../pixel-effect/45/4.png</key>
            <key type="filename">../pixel-effect/46/1.png</key>
            <key type="filename">../pixel-effect/46/2.png</key>
            <key type="filename">../pixel-effect/46/3.png</key>
            <key type="filename">../pixel-effect/46/4.png</key>
            <key type="filename">../pixel-effect/47/1.png</key>
            <key type="filename">../pixel-effect/47/2.png</key>
            <key type="filename">../pixel-effect/47/3.png</key>
            <key type="filename">../pixel-effect/47/4.png</key>
            <key type="filename">../pixel-effect/47/5.png</key>
            <key type="filename">../pixel-effect/47/6.png</key>
            <key type="filename">../pixel-effect/47/7.png</key>
            <key type="filename">../pixel-effect/47/8.png</key>
            <key type="filename">../pixel-effect/5/1.png</key>
            <key type="filename">../pixel-effect/5/2.png</key>
            <key type="filename">../pixel-effect/5/3.png</key>
            <key type="filename">../pixel-effect/5/4.png</key>
            <key type="filename">../pixel-effect/5/5.png</key>
            <key type="filename">../pixel-effect/5/6.png</key>
            <key type="filename">../pixel-effect/5/7.png</key>
            <key type="filename">../pixel-effect/6/1.png</key>
            <key type="filename">../pixel-effect/6/2.png</key>
            <key type="filename">../pixel-effect/6/3.png</key>
            <key type="filename">../pixel-effect/6/4.png</key>
            <key type="filename">../pixel-effect/6/5.png</key>
            <key type="filename">../pixel-effect/6/6.png</key>
            <key type="filename">../pixel-effect/7/1.png</key>
            <key type="filename">../pixel-effect/7/2.png</key>
            <key type="filename">../pixel-effect/7/3.png</key>
            <key type="filename">../pixel-effect/7/4.png</key>
            <key type="filename">../pixel-effect/7/5.png</key>
            <key type="filename">../pixel-effect/8/1.png</key>
            <key type="filename">../pixel-effect/8/2.png</key>
            <key type="filename">../pixel-effect/8/3.png</key>
            <key type="filename">../pixel-effect/8/4.png</key>
            <key type="filename">../pixel-effect/8/5.png</key>
            <key type="filename">../pixel-effect/9/1.png</key>
            <key type="filename">../pixel-effect/9/2.png</key>
            <key type="filename">../pixel-effect/9/3.png</key>
            <key type="filename">../pixel-effect/9/4.png</key>
            <key type="filename">../pixel-effect/9/5.png</key>
            <key type="filename">../pixel-effect/9/6.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../pixel-effect</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
    </struct>
</data>
