<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.2.3</string>
        <key>fileName</key>
        <string>/Volumes/CLALASD/art/spaceshooter/spaceshooter.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string>@4x</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.875</double>
                <key>extension</key>
                <string>@3_5x</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.75</double>
                <key>extension</key>
                <string>@3x</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.5</double>
                <key>extension</key>
                <string>@2x</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.25</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>corona-imagesheet</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>2</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>8192</int>
            <key>height</key>
            <int>8192</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>lua</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Project/shooting/sprites/spaceshooter{v}-{n1}.lua</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <true/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">dist/AlienUFOs/Ships/1.png</key>
            <key type="filename">dist/AlienUFOs/Ships/2.png</key>
            <key type="filename">dist/AlienUFOs/Ships/3.png</key>
            <key type="filename">dist/AlienUFOs/Ships/4.png</key>
            <key type="filename">dist/AlienUFOs/Ships/5.png</key>
            <key type="filename">dist/AlienUFOs/Ships/Damages/1.png</key>
            <key type="filename">dist/AlienUFOs/Ships/Damages/10.png</key>
            <key type="filename">dist/AlienUFOs/Ships/Damages/2.png</key>
            <key type="filename">dist/AlienUFOs/Ships/Damages/3.png</key>
            <key type="filename">dist/AlienUFOs/Ships/Damages/4.png</key>
            <key type="filename">dist/AlienUFOs/Ships/Damages/5.png</key>
            <key type="filename">dist/AlienUFOs/Ships/Damages/6.png</key>
            <key type="filename">dist/AlienUFOs/Ships/Damages/7.png</key>
            <key type="filename">dist/AlienUFOs/Ships/Damages/8.png</key>
            <key type="filename">dist/AlienUFOs/Ships/Damages/9.png</key>
            <key type="filename">dist/AlienUFOs/Ships/Parts/Legs/1.png</key>
            <key type="filename">dist/AlienUFOs/Ships/Parts/Legs/2.png</key>
            <key type="filename">dist/AlienUFOs/Ships/Parts/Legs/3.png</key>
            <key type="filename">dist/AlienUFOs/Ships/Parts/Legs/4.png</key>
            <key type="filename">dist/AlienUFOs/Ships/Parts/Main/1.png</key>
            <key type="filename">dist/AlienUFOs/Ships/Parts/Main/2.png</key>
            <key type="filename">dist/AlienUFOs/Ships/Parts/Main/3.png</key>
            <key type="filename">dist/AlienUFOs/Ships/Parts/Main/4.png</key>
            <key type="filename">dist/AlienUFOs/Ships/Parts/Main/5.png</key>
            <key type="filename">dist/AlienUFOs/Ships/Parts/dome.png</key>
            <key type="filename">dist/Aliens/Green/hurt.png</key>
            <key type="filename">dist/Aliens/Green/stand.png</key>
            <key type="filename">dist/Astronauts/1.png</key>
            <key type="filename">dist/Astronauts/10.png</key>
            <key type="filename">dist/Astronauts/11.png</key>
            <key type="filename">dist/Astronauts/12.png</key>
            <key type="filename">dist/Astronauts/13.png</key>
            <key type="filename">dist/Astronauts/14.png</key>
            <key type="filename">dist/Astronauts/15 Copy.png</key>
            <key type="filename">dist/Astronauts/15.png</key>
            <key type="filename">dist/Astronauts/16.png</key>
            <key type="filename">dist/Astronauts/17.png</key>
            <key type="filename">dist/Astronauts/18.png</key>
            <key type="filename">dist/Astronauts/2.png</key>
            <key type="filename">dist/Astronauts/3.png</key>
            <key type="filename">dist/Astronauts/4.png</key>
            <key type="filename">dist/Astronauts/5.png</key>
            <key type="filename">dist/Astronauts/6.png</key>
            <key type="filename">dist/Astronauts/7.png</key>
            <key type="filename">dist/Astronauts/8.png</key>
            <key type="filename">dist/Astronauts/9.png</key>
            <key type="filename">dist/Buildings/1.png</key>
            <key type="filename">dist/Buildings/10.png</key>
            <key type="filename">dist/Buildings/11.png</key>
            <key type="filename">dist/Buildings/12.png</key>
            <key type="filename">dist/Buildings/13 Copy.png</key>
            <key type="filename">dist/Buildings/13.png</key>
            <key type="filename">dist/Buildings/2.png</key>
            <key type="filename">dist/Buildings/3.png</key>
            <key type="filename">dist/Buildings/4.png</key>
            <key type="filename">dist/Buildings/5.png</key>
            <key type="filename">dist/Buildings/6.png</key>
            <key type="filename">dist/Buildings/7.png</key>
            <key type="filename">dist/Buildings/8.png</key>
            <key type="filename">dist/Buildings/9.png</key>
            <key type="filename">dist/Effects/Fire/1.png</key>
            <key type="filename">dist/Effects/Fire/10.png</key>
            <key type="filename">dist/Effects/Fire/11.png</key>
            <key type="filename">dist/Effects/Fire/12.png</key>
            <key type="filename">dist/Effects/Fire/13.png</key>
            <key type="filename">dist/Effects/Fire/14.png</key>
            <key type="filename">dist/Effects/Fire/15.png</key>
            <key type="filename">dist/Effects/Fire/16.png</key>
            <key type="filename">dist/Effects/Fire/2.png</key>
            <key type="filename">dist/Effects/Fire/3.png</key>
            <key type="filename">dist/Effects/Fire/4.png</key>
            <key type="filename">dist/Effects/Fire/5.png</key>
            <key type="filename">dist/Effects/Fire/6.png</key>
            <key type="filename">dist/Effects/Fire/7.png</key>
            <key type="filename">dist/Effects/Fire/8.png</key>
            <key type="filename">dist/Effects/Fire/9.png</key>
            <key type="filename">dist/Effects/Shield/1.png</key>
            <key type="filename">dist/Effects/Shield/2.png</key>
            <key type="filename">dist/Effects/Shield/3.png</key>
            <key type="filename">dist/Effects/Speed/1.png</key>
            <key type="filename">dist/Effects/Speed/2.png</key>
            <key type="filename">dist/Effects/Speed/3.png</key>
            <key type="filename">dist/Effects/Speed/4.png</key>
            <key type="filename">dist/Effects/Star/1.png</key>
            <key type="filename">dist/Effects/Star/2.png</key>
            <key type="filename">dist/Effects/Star/3.png</key>
            <key type="filename">dist/Effects/smoke/1.png</key>
            <key type="filename">dist/Effects/smoke/2.png</key>
            <key type="filename">dist/Effects/smoke/3.png</key>
            <key type="filename">dist/Effects/smoke/4.png</key>
            <key type="filename">dist/Effects/smoke/5.png</key>
            <key type="filename">dist/Effects/smoke/6.png</key>
            <key type="filename">dist/Effects/smoke/7.png</key>
            <key type="filename">dist/Effects/smoke/8.png</key>
            <key type="filename">dist/Effects/smoke/9.png</key>
            <key type="filename">dist/Items/1.png</key>
            <key type="filename">dist/Items/10.png</key>
            <key type="filename">dist/Items/100.png</key>
            <key type="filename">dist/Items/101.png</key>
            <key type="filename">dist/Items/102.png</key>
            <key type="filename">dist/Items/103.png</key>
            <key type="filename">dist/Items/104.png</key>
            <key type="filename">dist/Items/105.png</key>
            <key type="filename">dist/Items/106.png</key>
            <key type="filename">dist/Items/107.png</key>
            <key type="filename">dist/Items/108.png</key>
            <key type="filename">dist/Items/109.png</key>
            <key type="filename">dist/Items/11.png</key>
            <key type="filename">dist/Items/110.png</key>
            <key type="filename">dist/Items/111.png</key>
            <key type="filename">dist/Items/112.png</key>
            <key type="filename">dist/Items/113.png</key>
            <key type="filename">dist/Items/114.png</key>
            <key type="filename">dist/Items/115.png</key>
            <key type="filename">dist/Items/116.png</key>
            <key type="filename">dist/Items/117.png</key>
            <key type="filename">dist/Items/118.png</key>
            <key type="filename">dist/Items/119.png</key>
            <key type="filename">dist/Items/12.png</key>
            <key type="filename">dist/Items/120.png</key>
            <key type="filename">dist/Items/121.png</key>
            <key type="filename">dist/Items/122.png</key>
            <key type="filename">dist/Items/123.png</key>
            <key type="filename">dist/Items/124.png</key>
            <key type="filename">dist/Items/125.png</key>
            <key type="filename">dist/Items/126.png</key>
            <key type="filename">dist/Items/127.png</key>
            <key type="filename">dist/Items/128.png</key>
            <key type="filename">dist/Items/129.png</key>
            <key type="filename">dist/Items/13.png</key>
            <key type="filename">dist/Items/130.png</key>
            <key type="filename">dist/Items/131.png</key>
            <key type="filename">dist/Items/132.png</key>
            <key type="filename">dist/Items/133.png</key>
            <key type="filename">dist/Items/134.png</key>
            <key type="filename">dist/Items/135.png</key>
            <key type="filename">dist/Items/136.png</key>
            <key type="filename">dist/Items/137.png</key>
            <key type="filename">dist/Items/138.png</key>
            <key type="filename">dist/Items/139.png</key>
            <key type="filename">dist/Items/14.png</key>
            <key type="filename">dist/Items/140.png</key>
            <key type="filename">dist/Items/141.png</key>
            <key type="filename">dist/Items/142.png</key>
            <key type="filename">dist/Items/143.png</key>
            <key type="filename">dist/Items/144.png</key>
            <key type="filename">dist/Items/145.png</key>
            <key type="filename">dist/Items/146.png</key>
            <key type="filename">dist/Items/147.png</key>
            <key type="filename">dist/Items/148.png</key>
            <key type="filename">dist/Items/149.png</key>
            <key type="filename">dist/Items/15.png</key>
            <key type="filename">dist/Items/150.png</key>
            <key type="filename">dist/Items/151.png</key>
            <key type="filename">dist/Items/152.png</key>
            <key type="filename">dist/Items/153.png</key>
            <key type="filename">dist/Items/154.png</key>
            <key type="filename">dist/Items/155.png</key>
            <key type="filename">dist/Items/156.png</key>
            <key type="filename">dist/Items/157.png</key>
            <key type="filename">dist/Items/158.png</key>
            <key type="filename">dist/Items/159.png</key>
            <key type="filename">dist/Items/16.png</key>
            <key type="filename">dist/Items/160.png</key>
            <key type="filename">dist/Items/161.png</key>
            <key type="filename">dist/Items/162.png</key>
            <key type="filename">dist/Items/163.png</key>
            <key type="filename">dist/Items/17.png</key>
            <key type="filename">dist/Items/18.png</key>
            <key type="filename">dist/Items/19.png</key>
            <key type="filename">dist/Items/2.png</key>
            <key type="filename">dist/Items/20.png</key>
            <key type="filename">dist/Items/21.png</key>
            <key type="filename">dist/Items/22.png</key>
            <key type="filename">dist/Items/23.png</key>
            <key type="filename">dist/Items/24.png</key>
            <key type="filename">dist/Items/25.png</key>
            <key type="filename">dist/Items/26.png</key>
            <key type="filename">dist/Items/27.png</key>
            <key type="filename">dist/Items/28.png</key>
            <key type="filename">dist/Items/29.png</key>
            <key type="filename">dist/Items/3.png</key>
            <key type="filename">dist/Items/30.png</key>
            <key type="filename">dist/Items/31.png</key>
            <key type="filename">dist/Items/32.png</key>
            <key type="filename">dist/Items/33.png</key>
            <key type="filename">dist/Items/34.png</key>
            <key type="filename">dist/Items/35.png</key>
            <key type="filename">dist/Items/36.png</key>
            <key type="filename">dist/Items/37.png</key>
            <key type="filename">dist/Items/38.png</key>
            <key type="filename">dist/Items/39.png</key>
            <key type="filename">dist/Items/4.png</key>
            <key type="filename">dist/Items/40.png</key>
            <key type="filename">dist/Items/41.png</key>
            <key type="filename">dist/Items/42.png</key>
            <key type="filename">dist/Items/43.png</key>
            <key type="filename">dist/Items/44.png</key>
            <key type="filename">dist/Items/45.png</key>
            <key type="filename">dist/Items/46.png</key>
            <key type="filename">dist/Items/47.png</key>
            <key type="filename">dist/Items/48.png</key>
            <key type="filename">dist/Items/49.png</key>
            <key type="filename">dist/Items/5.png</key>
            <key type="filename">dist/Items/50.png</key>
            <key type="filename">dist/Items/51.png</key>
            <key type="filename">dist/Items/52.png</key>
            <key type="filename">dist/Items/53.png</key>
            <key type="filename">dist/Items/54.png</key>
            <key type="filename">dist/Items/55.png</key>
            <key type="filename">dist/Items/56.png</key>
            <key type="filename">dist/Items/57.png</key>
            <key type="filename">dist/Items/58.png</key>
            <key type="filename">dist/Items/59.png</key>
            <key type="filename">dist/Items/6.png</key>
            <key type="filename">dist/Items/60.png</key>
            <key type="filename">dist/Items/61.png</key>
            <key type="filename">dist/Items/62.png</key>
            <key type="filename">dist/Items/63.png</key>
            <key type="filename">dist/Items/64.png</key>
            <key type="filename">dist/Items/65.png</key>
            <key type="filename">dist/Items/66.png</key>
            <key type="filename">dist/Items/67.png</key>
            <key type="filename">dist/Items/68.png</key>
            <key type="filename">dist/Items/69.png</key>
            <key type="filename">dist/Items/7.png</key>
            <key type="filename">dist/Items/70.png</key>
            <key type="filename">dist/Items/71.png</key>
            <key type="filename">dist/Items/72.png</key>
            <key type="filename">dist/Items/73.png</key>
            <key type="filename">dist/Items/74.png</key>
            <key type="filename">dist/Items/75.png</key>
            <key type="filename">dist/Items/76.png</key>
            <key type="filename">dist/Items/77.png</key>
            <key type="filename">dist/Items/78.png</key>
            <key type="filename">dist/Items/79.png</key>
            <key type="filename">dist/Items/8.png</key>
            <key type="filename">dist/Items/80.png</key>
            <key type="filename">dist/Items/81.png</key>
            <key type="filename">dist/Items/82.png</key>
            <key type="filename">dist/Items/83.png</key>
            <key type="filename">dist/Items/84.png</key>
            <key type="filename">dist/Items/85.png</key>
            <key type="filename">dist/Items/86.png</key>
            <key type="filename">dist/Items/87.png</key>
            <key type="filename">dist/Items/88.png</key>
            <key type="filename">dist/Items/89.png</key>
            <key type="filename">dist/Items/9.png</key>
            <key type="filename">dist/Items/90.png</key>
            <key type="filename">dist/Items/91.png</key>
            <key type="filename">dist/Items/92.png</key>
            <key type="filename">dist/Items/93.png</key>
            <key type="filename">dist/Items/94.png</key>
            <key type="filename">dist/Items/95.png</key>
            <key type="filename">dist/Items/96.png</key>
            <key type="filename">dist/Items/97.png</key>
            <key type="filename">dist/Items/98.png</key>
            <key type="filename">dist/Items/99.png</key>
            <key type="filename">dist/Lasers/1.png</key>
            <key type="filename">dist/Lasers/10.png</key>
            <key type="filename">dist/Lasers/11.png</key>
            <key type="filename">dist/Lasers/12.png</key>
            <key type="filename">dist/Lasers/13.png</key>
            <key type="filename">dist/Lasers/14.png</key>
            <key type="filename">dist/Lasers/15.png</key>
            <key type="filename">dist/Lasers/16.png</key>
            <key type="filename">dist/Lasers/17.png</key>
            <key type="filename">dist/Lasers/18.png</key>
            <key type="filename">dist/Lasers/19.png</key>
            <key type="filename">dist/Lasers/2.png</key>
            <key type="filename">dist/Lasers/20.png</key>
            <key type="filename">dist/Lasers/21.png</key>
            <key type="filename">dist/Lasers/22.png</key>
            <key type="filename">dist/Lasers/23.png</key>
            <key type="filename">dist/Lasers/24.png</key>
            <key type="filename">dist/Lasers/25.png</key>
            <key type="filename">dist/Lasers/26.png</key>
            <key type="filename">dist/Lasers/27.png</key>
            <key type="filename">dist/Lasers/28.png</key>
            <key type="filename">dist/Lasers/29.png</key>
            <key type="filename">dist/Lasers/3.png</key>
            <key type="filename">dist/Lasers/30.png</key>
            <key type="filename">dist/Lasers/31.png</key>
            <key type="filename">dist/Lasers/32.png</key>
            <key type="filename">dist/Lasers/33.png</key>
            <key type="filename">dist/Lasers/34.png</key>
            <key type="filename">dist/Lasers/35.png</key>
            <key type="filename">dist/Lasers/36.png</key>
            <key type="filename">dist/Lasers/37.png</key>
            <key type="filename">dist/Lasers/38.png</key>
            <key type="filename">dist/Lasers/39.png</key>
            <key type="filename">dist/Lasers/4.png</key>
            <key type="filename">dist/Lasers/40.png</key>
            <key type="filename">dist/Lasers/41.png</key>
            <key type="filename">dist/Lasers/42.png</key>
            <key type="filename">dist/Lasers/43.png</key>
            <key type="filename">dist/Lasers/44.png</key>
            <key type="filename">dist/Lasers/45.png</key>
            <key type="filename">dist/Lasers/46.png</key>
            <key type="filename">dist/Lasers/47.png</key>
            <key type="filename">dist/Lasers/48.png</key>
            <key type="filename">dist/Lasers/5.png</key>
            <key type="filename">dist/Lasers/6.png</key>
            <key type="filename">dist/Lasers/7.png</key>
            <key type="filename">dist/Lasers/8.png</key>
            <key type="filename">dist/Lasers/9.png</key>
            <key type="filename">dist/Lasers/Beam/1.png</key>
            <key type="filename">dist/Lasers/Beam/2.png</key>
            <key type="filename">dist/Lasers/Beam/3.png</key>
            <key type="filename">dist/Lasers/Beam/4.png</key>
            <key type="filename">dist/Lasers/Beam/5.png</key>
            <key type="filename">dist/Lasers/Burst/1.png</key>
            <key type="filename">dist/Lasers/Burst/2.png</key>
            <key type="filename">dist/Lasers/Burst/3.png</key>
            <key type="filename">dist/Lasers/Burst/4 Copy.png</key>
            <key type="filename">dist/Lasers/Burst/4.png</key>
            <key type="filename">dist/Lasers/Burst/5.png</key>
            <key type="filename">dist/Lasers/GroundBurst/1.png</key>
            <key type="filename">dist/Lasers/GroundBurst/2.png</key>
            <key type="filename">dist/Lasers/GroundBurst/3.png</key>
            <key type="filename">dist/Lasers/GroundBurst/4.png</key>
            <key type="filename">dist/Lasers/GroundBurst/5.png</key>
            <key type="filename">dist/Lasers/Rings/1.png</key>
            <key type="filename">dist/Lasers/Rings/2.png</key>
            <key type="filename">dist/Lasers/Rings/3.png</key>
            <key type="filename">dist/Lasers/Rings/4.png</key>
            <key type="filename">dist/Lasers/Rings/5.png</key>
            <key type="filename">dist/Logo.png</key>
            <key type="filename">dist/Meteors/Big/1.png</key>
            <key type="filename">dist/Meteors/Big/2.png</key>
            <key type="filename">dist/Meteors/Big/3.png</key>
            <key type="filename">dist/Meteors/Big/4.png</key>
            <key type="filename">dist/Meteors/Big/5.png</key>
            <key type="filename">dist/Meteors/Big/6.png</key>
            <key type="filename">dist/Meteors/Big/7.png</key>
            <key type="filename">dist/Meteors/Big/8.png</key>
            <key type="filename">dist/Meteors/Huge/1.png</key>
            <key type="filename">dist/Meteors/Huge/2.png</key>
            <key type="filename">dist/Meteors/Huge/3.png</key>
            <key type="filename">dist/Meteors/Huge/4.png</key>
            <key type="filename">dist/Meteors/Med/1.png</key>
            <key type="filename">dist/Meteors/Med/2.png</key>
            <key type="filename">dist/Meteors/Med/3.png</key>
            <key type="filename">dist/Meteors/Med/4.png</key>
            <key type="filename">dist/Meteors/Small/1.png</key>
            <key type="filename">dist/Meteors/Small/2.png</key>
            <key type="filename">dist/Meteors/Small/3.png</key>
            <key type="filename">dist/Meteors/Small/4.png</key>
            <key type="filename">dist/Meteors/Tiny/1.png</key>
            <key type="filename">dist/Meteors/Tiny/2.png</key>
            <key type="filename">dist/Meteors/Tiny/3.png</key>
            <key type="filename">dist/Meteors/Tiny/4.png</key>
            <key type="filename">dist/Missiles/1.png</key>
            <key type="filename">dist/Missiles/10.png</key>
            <key type="filename">dist/Missiles/11.png</key>
            <key type="filename">dist/Missiles/12.png</key>
            <key type="filename">dist/Missiles/13.png</key>
            <key type="filename">dist/Missiles/14.png</key>
            <key type="filename">dist/Missiles/15.png</key>
            <key type="filename">dist/Missiles/16.png</key>
            <key type="filename">dist/Missiles/17.png</key>
            <key type="filename">dist/Missiles/18.png</key>
            <key type="filename">dist/Missiles/19.png</key>
            <key type="filename">dist/Missiles/2.png</key>
            <key type="filename">dist/Missiles/20.png</key>
            <key type="filename">dist/Missiles/21.png</key>
            <key type="filename">dist/Missiles/22.png</key>
            <key type="filename">dist/Missiles/23.png</key>
            <key type="filename">dist/Missiles/24.png</key>
            <key type="filename">dist/Missiles/25.png</key>
            <key type="filename">dist/Missiles/26.png</key>
            <key type="filename">dist/Missiles/27.png</key>
            <key type="filename">dist/Missiles/28.png</key>
            <key type="filename">dist/Missiles/29.png</key>
            <key type="filename">dist/Missiles/3.png</key>
            <key type="filename">dist/Missiles/30.png</key>
            <key type="filename">dist/Missiles/31.png</key>
            <key type="filename">dist/Missiles/32.png</key>
            <key type="filename">dist/Missiles/33.png</key>
            <key type="filename">dist/Missiles/34.png</key>
            <key type="filename">dist/Missiles/35.png</key>
            <key type="filename">dist/Missiles/36.png</key>
            <key type="filename">dist/Missiles/37.png</key>
            <key type="filename">dist/Missiles/38.png</key>
            <key type="filename">dist/Missiles/39.png</key>
            <key type="filename">dist/Missiles/4.png</key>
            <key type="filename">dist/Missiles/40.png</key>
            <key type="filename">dist/Missiles/5.png</key>
            <key type="filename">dist/Missiles/6.png</key>
            <key type="filename">dist/Missiles/7.png</key>
            <key type="filename">dist/Missiles/8.png</key>
            <key type="filename">dist/Missiles/9.png</key>
            <key type="filename">dist/Particles/1.png</key>
            <key type="filename">dist/Particles/10.png</key>
            <key type="filename">dist/Particles/11.png</key>
            <key type="filename">dist/Particles/12.png</key>
            <key type="filename">dist/Particles/13.png</key>
            <key type="filename">dist/Particles/14.png</key>
            <key type="filename">dist/Particles/15.png</key>
            <key type="filename">dist/Particles/16.png</key>
            <key type="filename">dist/Particles/17.png</key>
            <key type="filename">dist/Particles/18.png</key>
            <key type="filename">dist/Particles/19.png</key>
            <key type="filename">dist/Particles/2.png</key>
            <key type="filename">dist/Particles/20.png</key>
            <key type="filename">dist/Particles/21.png</key>
            <key type="filename">dist/Particles/3.png</key>
            <key type="filename">dist/Particles/4.png</key>
            <key type="filename">dist/Particles/5.png</key>
            <key type="filename">dist/Particles/6.png</key>
            <key type="filename">dist/Particles/7.png</key>
            <key type="filename">dist/Particles/8.png</key>
            <key type="filename">dist/Particles/9.png</key>
            <key type="filename">dist/Planet/1.png</key>
            <key type="filename">dist/Planet/2.png</key>
            <key type="filename">dist/Planet/3.png</key>
            <key type="filename">dist/Planet/4.png</key>
            <key type="filename">dist/Planet/5.png</key>
            <key type="filename">dist/Planet/6.png</key>
            <key type="filename">dist/Planet/7.png</key>
            <key type="filename">dist/Planet/8.png</key>
            <key type="filename">dist/Planet/9.png</key>
            <key type="filename">dist/Power-ups/Bolt/1.png</key>
            <key type="filename">dist/Power-ups/Bolt/2.png</key>
            <key type="filename">dist/Power-ups/Bolt/3.png</key>
            <key type="filename">dist/Power-ups/Bolt/4.png</key>
            <key type="filename">dist/Power-ups/Bolt/5.png</key>
            <key type="filename">dist/Power-ups/Bolt/6.png</key>
            <key type="filename">dist/Power-ups/Bolt/7.png</key>
            <key type="filename">dist/Power-ups/None/1.png</key>
            <key type="filename">dist/Power-ups/None/2.png</key>
            <key type="filename">dist/Power-ups/None/3.png</key>
            <key type="filename">dist/Power-ups/None/4.png</key>
            <key type="filename">dist/Power-ups/Pills/1.png</key>
            <key type="filename">dist/Power-ups/Pills/2.png</key>
            <key type="filename">dist/Power-ups/Pills/3.png</key>
            <key type="filename">dist/Power-ups/Pills/4.png</key>
            <key type="filename">dist/Power-ups/Shield/1.png</key>
            <key type="filename">dist/Power-ups/Shield/2.png</key>
            <key type="filename">dist/Power-ups/Shield/3.png</key>
            <key type="filename">dist/Power-ups/Shield/4.png</key>
            <key type="filename">dist/Power-ups/Shield/5.png</key>
            <key type="filename">dist/Power-ups/Shield/6.png</key>
            <key type="filename">dist/Power-ups/Shield/7.png</key>
            <key type="filename">dist/Power-ups/Star/1.png</key>
            <key type="filename">dist/Power-ups/Star/2.png</key>
            <key type="filename">dist/Power-ups/Star/3.png</key>
            <key type="filename">dist/Power-ups/Star/4.png</key>
            <key type="filename">dist/Power-ups/Star/5.png</key>
            <key type="filename">dist/Power-ups/Star/6.png</key>
            <key type="filename">dist/Power-ups/Star/7.png</key>
            <key type="filename">dist/Power-ups/Things/1.png</key>
            <key type="filename">dist/Power-ups/Things/2.png</key>
            <key type="filename">dist/Power-ups/Things/3.png</key>
            <key type="filename">dist/Rockets/Parts/1.png</key>
            <key type="filename">dist/Rockets/Parts/10.png</key>
            <key type="filename">dist/Rockets/Parts/11.png</key>
            <key type="filename">dist/Rockets/Parts/12.png</key>
            <key type="filename">dist/Rockets/Parts/13.png</key>
            <key type="filename">dist/Rockets/Parts/14.png</key>
            <key type="filename">dist/Rockets/Parts/15.png</key>
            <key type="filename">dist/Rockets/Parts/16.png</key>
            <key type="filename">dist/Rockets/Parts/17.png</key>
            <key type="filename">dist/Rockets/Parts/18.png</key>
            <key type="filename">dist/Rockets/Parts/19.png</key>
            <key type="filename">dist/Rockets/Parts/2.png</key>
            <key type="filename">dist/Rockets/Parts/20.png</key>
            <key type="filename">dist/Rockets/Parts/21.png</key>
            <key type="filename">dist/Rockets/Parts/22.png</key>
            <key type="filename">dist/Rockets/Parts/23.png</key>
            <key type="filename">dist/Rockets/Parts/24.png</key>
            <key type="filename">dist/Rockets/Parts/25.png</key>
            <key type="filename">dist/Rockets/Parts/26.png</key>
            <key type="filename">dist/Rockets/Parts/27.png</key>
            <key type="filename">dist/Rockets/Parts/28.png</key>
            <key type="filename">dist/Rockets/Parts/29.png</key>
            <key type="filename">dist/Rockets/Parts/3.png</key>
            <key type="filename">dist/Rockets/Parts/30.png</key>
            <key type="filename">dist/Rockets/Parts/31.png</key>
            <key type="filename">dist/Rockets/Parts/4.png</key>
            <key type="filename">dist/Rockets/Parts/5.png</key>
            <key type="filename">dist/Rockets/Parts/6.png</key>
            <key type="filename">dist/Rockets/Parts/7.png</key>
            <key type="filename">dist/Rockets/Parts/8.png</key>
            <key type="filename">dist/Rockets/Parts/9.png</key>
            <key type="filename">dist/Rockets/SpaceRockets/1.png</key>
            <key type="filename">dist/Rockets/SpaceRockets/2.png</key>
            <key type="filename">dist/Rockets/SpaceRockets/3 Copy.png</key>
            <key type="filename">dist/Rockets/SpaceRockets/3.png</key>
            <key type="filename">dist/Rockets/SpaceRockets/4.png</key>
            <key type="filename">dist/Ruby/1.png</key>
            <key type="filename">dist/Ruby/2.png</key>
            <key type="filename">dist/Ruby/3.png</key>
            <key type="filename">dist/Ruby/4.png</key>
            <key type="filename">dist/Ships/1.png</key>
            <key type="filename">dist/Ships/10.png</key>
            <key type="filename">dist/Ships/11.png</key>
            <key type="filename">dist/Ships/12.png</key>
            <key type="filename">dist/Ships/13.png</key>
            <key type="filename">dist/Ships/14.png</key>
            <key type="filename">dist/Ships/15.png</key>
            <key type="filename">dist/Ships/16.png</key>
            <key type="filename">dist/Ships/17.png</key>
            <key type="filename">dist/Ships/18.png</key>
            <key type="filename">dist/Ships/19.png</key>
            <key type="filename">dist/Ships/2.png</key>
            <key type="filename">dist/Ships/20.png</key>
            <key type="filename">dist/Ships/21.png</key>
            <key type="filename">dist/Ships/22.png</key>
            <key type="filename">dist/Ships/23.png</key>
            <key type="filename">dist/Ships/24.png</key>
            <key type="filename">dist/Ships/25.png</key>
            <key type="filename">dist/Ships/26.png</key>
            <key type="filename">dist/Ships/27.png</key>
            <key type="filename">dist/Ships/28.png</key>
            <key type="filename">dist/Ships/29.png</key>
            <key type="filename">dist/Ships/3.png</key>
            <key type="filename">dist/Ships/30.png</key>
            <key type="filename">dist/Ships/31.png</key>
            <key type="filename">dist/Ships/32.png</key>
            <key type="filename">dist/Ships/33.png</key>
            <key type="filename">dist/Ships/34.png</key>
            <key type="filename">dist/Ships/35.png</key>
            <key type="filename">dist/Ships/36.png</key>
            <key type="filename">dist/Ships/37.png</key>
            <key type="filename">dist/Ships/38.png</key>
            <key type="filename">dist/Ships/39.png</key>
            <key type="filename">dist/Ships/4.png</key>
            <key type="filename">dist/Ships/40.png</key>
            <key type="filename">dist/Ships/41.png</key>
            <key type="filename">dist/Ships/5.png</key>
            <key type="filename">dist/Ships/6.png</key>
            <key type="filename">dist/Ships/7.png</key>
            <key type="filename">dist/Ships/8.png</key>
            <key type="filename">dist/Ships/9.png</key>
            <key type="filename">dist/Ships/Damages/1.png</key>
            <key type="filename">dist/Ships/Damages/2.png</key>
            <key type="filename">dist/Ships/Damages/3.png</key>
            <key type="filename">dist/Ships/Damages/4.png</key>
            <key type="filename">dist/Ships/Damages/5.png</key>
            <key type="filename">dist/Ships/Damages/6.png</key>
            <key type="filename">dist/Ships/Damages/7.png</key>
            <key type="filename">dist/Ships/Damages/8.png</key>
            <key type="filename">dist/Ships/Damages/9.png</key>
            <key type="filename">dist/Ships/Parts/Beams/1.png</key>
            <key type="filename">dist/Ships/Parts/Beams/10.png</key>
            <key type="filename">dist/Ships/Parts/Beams/11.png</key>
            <key type="filename">dist/Ships/Parts/Beams/12.png</key>
            <key type="filename">dist/Ships/Parts/Beams/13.png</key>
            <key type="filename">dist/Ships/Parts/Beams/14.png</key>
            <key type="filename">dist/Ships/Parts/Beams/2.png</key>
            <key type="filename">dist/Ships/Parts/Beams/3.png</key>
            <key type="filename">dist/Ships/Parts/Beams/4.png</key>
            <key type="filename">dist/Ships/Parts/Beams/5.png</key>
            <key type="filename">dist/Ships/Parts/Beams/6.png</key>
            <key type="filename">dist/Ships/Parts/Beams/7.png</key>
            <key type="filename">dist/Ships/Parts/Beams/8.png</key>
            <key type="filename">dist/Ships/Parts/Beams/9.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/1.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/10.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/11.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/12.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/13.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/14.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/15.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/16.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/17.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/18.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/19.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/2.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/20.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/21.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/22.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/23.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/24.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/25.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/26.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/27.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/28.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/29.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/3.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/30.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/31.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/32.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/4.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/5.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/6.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/7.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/8.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/9.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Bases/1.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Bases/10.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Bases/11.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Bases/12.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Bases/13.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Bases/14.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Bases/15.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Bases/16.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Bases/17.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Bases/18.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Bases/2.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Bases/3.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Bases/4.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Bases/5.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Bases/6.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Bases/7.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Bases/8.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Bases/9.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Glass/1.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Glass/10.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Glass/11.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Glass/12.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Glass/13.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Glass/14.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Glass/15.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Glass/16.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Glass/17.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Glass/18.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Glass/19.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Glass/2.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Glass/20.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Glass/21.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Glass/22.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Glass/23.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Glass/24.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Glass/25.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Glass/26.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Glass/3.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Glass/4.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Glass/5.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Glass/6.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Glass/7.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Glass/8.png</key>
            <key type="filename">dist/Ships/Parts/Cockpits/Glass/9.png</key>
            <key type="filename">dist/Ships/Parts/Engines/1.png</key>
            <key type="filename">dist/Ships/Parts/Engines/2.png</key>
            <key type="filename">dist/Ships/Parts/Engines/3.png</key>
            <key type="filename">dist/Ships/Parts/Engines/4.png</key>
            <key type="filename">dist/Ships/Parts/Engines/5.png</key>
            <key type="filename">dist/Ships/Parts/Engines/6.png</key>
            <key type="filename">dist/Ships/Parts/Engines/7.png</key>
            <key type="filename">dist/Ships/Parts/Engines/8.png</key>
            <key type="filename">dist/Ships/Parts/Engines/9.png</key>
            <key type="filename">dist/Ships/Parts/Guns/1.png</key>
            <key type="filename">dist/Ships/Parts/Guns/10.png</key>
            <key type="filename">dist/Ships/Parts/Guns/11.png</key>
            <key type="filename">dist/Ships/Parts/Guns/12.png</key>
            <key type="filename">dist/Ships/Parts/Guns/13.png</key>
            <key type="filename">dist/Ships/Parts/Guns/14.png</key>
            <key type="filename">dist/Ships/Parts/Guns/15.png</key>
            <key type="filename">dist/Ships/Parts/Guns/2.png</key>
            <key type="filename">dist/Ships/Parts/Guns/3.png</key>
            <key type="filename">dist/Ships/Parts/Guns/4.png</key>
            <key type="filename">dist/Ships/Parts/Guns/5.png</key>
            <key type="filename">dist/Ships/Parts/Guns/6.png</key>
            <key type="filename">dist/Ships/Parts/Guns/7.png</key>
            <key type="filename">dist/Ships/Parts/Guns/8.png</key>
            <key type="filename">dist/Ships/Parts/Guns/9.png</key>
            <key type="filename">dist/Ships/Parts/Others/1.png</key>
            <key type="filename">dist/Ships/Parts/Others/2.png</key>
            <key type="filename">dist/Ships/Parts/Others/3.png</key>
            <key type="filename">dist/Ships/Parts/Others/4.png</key>
            <key type="filename">dist/Ships/Parts/Others/5.png</key>
            <key type="filename">dist/Ships/Parts/Scratches/1.png</key>
            <key type="filename">dist/Ships/Parts/Scratches/2.png</key>
            <key type="filename">dist/Ships/Parts/Wings/1.png</key>
            <key type="filename">dist/Ships/Parts/Wings/10.png</key>
            <key type="filename">dist/Ships/Parts/Wings/11.png</key>
            <key type="filename">dist/Ships/Parts/Wings/12.png</key>
            <key type="filename">dist/Ships/Parts/Wings/13.png</key>
            <key type="filename">dist/Ships/Parts/Wings/14.png</key>
            <key type="filename">dist/Ships/Parts/Wings/15.png</key>
            <key type="filename">dist/Ships/Parts/Wings/16.png</key>
            <key type="filename">dist/Ships/Parts/Wings/17.png</key>
            <key type="filename">dist/Ships/Parts/Wings/18.png</key>
            <key type="filename">dist/Ships/Parts/Wings/19.png</key>
            <key type="filename">dist/Ships/Parts/Wings/2.png</key>
            <key type="filename">dist/Ships/Parts/Wings/20.png</key>
            <key type="filename">dist/Ships/Parts/Wings/21.png</key>
            <key type="filename">dist/Ships/Parts/Wings/22.png</key>
            <key type="filename">dist/Ships/Parts/Wings/23.png</key>
            <key type="filename">dist/Ships/Parts/Wings/24.png</key>
            <key type="filename">dist/Ships/Parts/Wings/25.png</key>
            <key type="filename">dist/Ships/Parts/Wings/26.png</key>
            <key type="filename">dist/Ships/Parts/Wings/27.png</key>
            <key type="filename">dist/Ships/Parts/Wings/28.png</key>
            <key type="filename">dist/Ships/Parts/Wings/29.png</key>
            <key type="filename">dist/Ships/Parts/Wings/3.png</key>
            <key type="filename">dist/Ships/Parts/Wings/30.png</key>
            <key type="filename">dist/Ships/Parts/Wings/31.png</key>
            <key type="filename">dist/Ships/Parts/Wings/32.png</key>
            <key type="filename">dist/Ships/Parts/Wings/33.png</key>
            <key type="filename">dist/Ships/Parts/Wings/34.png</key>
            <key type="filename">dist/Ships/Parts/Wings/35.png</key>
            <key type="filename">dist/Ships/Parts/Wings/36.png</key>
            <key type="filename">dist/Ships/Parts/Wings/37.png</key>
            <key type="filename">dist/Ships/Parts/Wings/38.png</key>
            <key type="filename">dist/Ships/Parts/Wings/39.png</key>
            <key type="filename">dist/Ships/Parts/Wings/4.png</key>
            <key type="filename">dist/Ships/Parts/Wings/40.png</key>
            <key type="filename">dist/Ships/Parts/Wings/41.png</key>
            <key type="filename">dist/Ships/Parts/Wings/42.png</key>
            <key type="filename">dist/Ships/Parts/Wings/43.png</key>
            <key type="filename">dist/Ships/Parts/Wings/44.png</key>
            <key type="filename">dist/Ships/Parts/Wings/45.png</key>
            <key type="filename">dist/Ships/Parts/Wings/46.png</key>
            <key type="filename">dist/Ships/Parts/Wings/47.png</key>
            <key type="filename">dist/Ships/Parts/Wings/48.png</key>
            <key type="filename">dist/Ships/Parts/Wings/49.png</key>
            <key type="filename">dist/Ships/Parts/Wings/5.png</key>
            <key type="filename">dist/Ships/Parts/Wings/50.png</key>
            <key type="filename">dist/Ships/Parts/Wings/51.png</key>
            <key type="filename">dist/Ships/Parts/Wings/52.png</key>
            <key type="filename">dist/Ships/Parts/Wings/53.png</key>
            <key type="filename">dist/Ships/Parts/Wings/54.png</key>
            <key type="filename">dist/Ships/Parts/Wings/55.png</key>
            <key type="filename">dist/Ships/Parts/Wings/56.png</key>
            <key type="filename">dist/Ships/Parts/Wings/57.png</key>
            <key type="filename">dist/Ships/Parts/Wings/58.png</key>
            <key type="filename">dist/Ships/Parts/Wings/59.png</key>
            <key type="filename">dist/Ships/Parts/Wings/6.png</key>
            <key type="filename">dist/Ships/Parts/Wings/60.png</key>
            <key type="filename">dist/Ships/Parts/Wings/61.png</key>
            <key type="filename">dist/Ships/Parts/Wings/62.png</key>
            <key type="filename">dist/Ships/Parts/Wings/63.png</key>
            <key type="filename">dist/Ships/Parts/Wings/64.png</key>
            <key type="filename">dist/Ships/Parts/Wings/65.png</key>
            <key type="filename">dist/Ships/Parts/Wings/66.png</key>
            <key type="filename">dist/Ships/Parts/Wings/67.png</key>
            <key type="filename">dist/Ships/Parts/Wings/68.png</key>
            <key type="filename">dist/Ships/Parts/Wings/7.png</key>
            <key type="filename">dist/Ships/Parts/Wings/8.png</key>
            <key type="filename">dist/Ships/Parts/Wings/9.png</key>
            <key type="filename">dist/Stations/1.png</key>
            <key type="filename">dist/Stations/10.png</key>
            <key type="filename">dist/Stations/11.png</key>
            <key type="filename">dist/Stations/2.png</key>
            <key type="filename">dist/Stations/3.png</key>
            <key type="filename">dist/Stations/4.png</key>
            <key type="filename">dist/Stations/5.png</key>
            <key type="filename">dist/Stations/6.png</key>
            <key type="filename">dist/Stations/7.png</key>
            <key type="filename">dist/Stations/8.png</key>
            <key type="filename">dist/Stations/9.png</key>
            <key type="filename">dist/Stations/Parts/1.png</key>
            <key type="filename">dist/Stations/Parts/10.png</key>
            <key type="filename">dist/Stations/Parts/11.png</key>
            <key type="filename">dist/Stations/Parts/12.png</key>
            <key type="filename">dist/Stations/Parts/13.png</key>
            <key type="filename">dist/Stations/Parts/14.png</key>
            <key type="filename">dist/Stations/Parts/15.png</key>
            <key type="filename">dist/Stations/Parts/16.png</key>
            <key type="filename">dist/Stations/Parts/17.png</key>
            <key type="filename">dist/Stations/Parts/18.png</key>
            <key type="filename">dist/Stations/Parts/19.png</key>
            <key type="filename">dist/Stations/Parts/2.png</key>
            <key type="filename">dist/Stations/Parts/20.png</key>
            <key type="filename">dist/Stations/Parts/21.png</key>
            <key type="filename">dist/Stations/Parts/22.png</key>
            <key type="filename">dist/Stations/Parts/23.png</key>
            <key type="filename">dist/Stations/Parts/24.png</key>
            <key type="filename">dist/Stations/Parts/3.png</key>
            <key type="filename">dist/Stations/Parts/4.png</key>
            <key type="filename">dist/Stations/Parts/5.png</key>
            <key type="filename">dist/Stations/Parts/6.png</key>
            <key type="filename">dist/Stations/Parts/7.png</key>
            <key type="filename">dist/Stations/Parts/8.png</key>
            <key type="filename">dist/Stations/Parts/9.png</key>
            <key type="filename">dist/Turrets/1.png</key>
            <key type="filename">dist/Turrets/2.png</key>
            <key type="filename">dist/Turrets/3.png</key>
            <key type="filename">dist/Turrets/Parts/Bases/1.png</key>
            <key type="filename">dist/Turrets/Parts/Bases/2.png</key>
            <key type="filename">dist/Turrets/Parts/Bases/3.png</key>
            <key type="filename">dist/Turrets/Parts/Bases/4.png</key>
            <key type="filename">dist/Turrets/Parts/Cannos/1.png</key>
            <key type="filename">dist/Turrets/Parts/Cannos/2.png</key>
            <key type="filename">dist/Turrets/Parts/Cannos/3.png</key>
            <key type="filename">dist/UFOs/1.png</key>
            <key type="filename">dist/UFOs/2.png</key>
            <key type="filename">dist/UFOs/3.png</key>
            <key type="filename">dist/UFOs/4.png</key>
            <key type="filename">dist/UI/Bar-corners/1/3-slices/1.png</key>
            <key type="filename">dist/UI/Bar-corners/1/3-slices/2.png</key>
            <key type="filename">dist/UI/Bar-corners/1/3-slices/3.png</key>
            <key type="filename">dist/UI/Bar-corners/2/3-slices/1.png</key>
            <key type="filename">dist/UI/Bar-corners/2/3-slices/2.png</key>
            <key type="filename">dist/UI/Bar-corners/2/3-slices/3.png</key>
            <key type="filename">dist/UI/Bar-corners/3/3-slices/1.png</key>
            <key type="filename">dist/UI/Bar-corners/3/3-slices/2.png</key>
            <key type="filename">dist/UI/Bar-corners/3/3-slices/3.png</key>
            <key type="filename">dist/UI/Bar-corners/4/3-slices/1.png</key>
            <key type="filename">dist/UI/Bar-corners/4/3-slices/2.png</key>
            <key type="filename">dist/UI/Bar-corners/4/3-slices/3.png</key>
            <key type="filename">dist/UI/Bars/1/9-slices/1.png</key>
            <key type="filename">dist/UI/Bars/1/9-slices/2.png</key>
            <key type="filename">dist/UI/Bars/1/9-slices/3.png</key>
            <key type="filename">dist/UI/Bars/1/9-slices/4.png</key>
            <key type="filename">dist/UI/Bars/1/9-slices/5.png</key>
            <key type="filename">dist/UI/Bars/1/9-slices/6.png</key>
            <key type="filename">dist/UI/Bars/1/9-slices/7.png</key>
            <key type="filename">dist/UI/Bars/1/9-slices/8.png</key>
            <key type="filename">dist/UI/Bars/1/9-slices/9.png</key>
            <key type="filename">dist/UI/Bars/2/9-slices/1.png</key>
            <key type="filename">dist/UI/Bars/2/9-slices/2.png</key>
            <key type="filename">dist/UI/Bars/2/9-slices/3.png</key>
            <key type="filename">dist/UI/Bars/2/9-slices/4.png</key>
            <key type="filename">dist/UI/Bars/2/9-slices/5.png</key>
            <key type="filename">dist/UI/Bars/2/9-slices/6.png</key>
            <key type="filename">dist/UI/Bars/2/9-slices/7.png</key>
            <key type="filename">dist/UI/Bars/2/9-slices/8.png</key>
            <key type="filename">dist/UI/Bars/2/9-slices/9.png</key>
            <key type="filename">dist/UI/Bars/3/9-slices/1.png</key>
            <key type="filename">dist/UI/Bars/3/9-slices/2.png</key>
            <key type="filename">dist/UI/Bars/3/9-slices/3.png</key>
            <key type="filename">dist/UI/Bars/3/9-slices/4.png</key>
            <key type="filename">dist/UI/Bars/3/9-slices/5.png</key>
            <key type="filename">dist/UI/Bars/3/9-slices/6.png</key>
            <key type="filename">dist/UI/Bars/3/9-slices/7.png</key>
            <key type="filename">dist/UI/Bars/3/9-slices/8.png</key>
            <key type="filename">dist/UI/Bars/3/9-slices/9.png</key>
            <key type="filename">dist/UI/Bars/4/9-slices/1.png</key>
            <key type="filename">dist/UI/Bars/4/9-slices/2.png</key>
            <key type="filename">dist/UI/Bars/4/9-slices/3.png</key>
            <key type="filename">dist/UI/Bars/4/9-slices/4.png</key>
            <key type="filename">dist/UI/Bars/4/9-slices/5.png</key>
            <key type="filename">dist/UI/Bars/4/9-slices/6.png</key>
            <key type="filename">dist/UI/Bars/4/9-slices/7.png</key>
            <key type="filename">dist/UI/Bars/4/9-slices/8.png</key>
            <key type="filename">dist/UI/Bars/4/9-slices/9.png</key>
            <key type="filename">dist/UI/Buttons/1/9-slices/1.png</key>
            <key type="filename">dist/UI/Buttons/1/9-slices/2.png</key>
            <key type="filename">dist/UI/Buttons/1/9-slices/3.png</key>
            <key type="filename">dist/UI/Buttons/1/9-slices/4.png</key>
            <key type="filename">dist/UI/Buttons/1/9-slices/5.png</key>
            <key type="filename">dist/UI/Buttons/1/9-slices/6.png</key>
            <key type="filename">dist/UI/Buttons/1/9-slices/7.png</key>
            <key type="filename">dist/UI/Buttons/1/9-slices/8.png</key>
            <key type="filename">dist/UI/Buttons/1/9-slices/9.png</key>
            <key type="filename">dist/UI/Buttons/2/9-slices/1.png</key>
            <key type="filename">dist/UI/Buttons/2/9-slices/2.png</key>
            <key type="filename">dist/UI/Buttons/2/9-slices/3.png</key>
            <key type="filename">dist/UI/Buttons/2/9-slices/4.png</key>
            <key type="filename">dist/UI/Buttons/2/9-slices/5.png</key>
            <key type="filename">dist/UI/Buttons/2/9-slices/6.png</key>
            <key type="filename">dist/UI/Buttons/2/9-slices/7.png</key>
            <key type="filename">dist/UI/Buttons/2/9-slices/8.png</key>
            <key type="filename">dist/UI/Buttons/2/9-slices/9.png</key>
            <key type="filename">dist/UI/Buttons/3/9-slices/1.png</key>
            <key type="filename">dist/UI/Buttons/3/9-slices/2.png</key>
            <key type="filename">dist/UI/Buttons/3/9-slices/3.png</key>
            <key type="filename">dist/UI/Buttons/3/9-slices/4.png</key>
            <key type="filename">dist/UI/Buttons/3/9-slices/5.png</key>
            <key type="filename">dist/UI/Buttons/3/9-slices/6.png</key>
            <key type="filename">dist/UI/Buttons/3/9-slices/7.png</key>
            <key type="filename">dist/UI/Buttons/3/9-slices/8.png</key>
            <key type="filename">dist/UI/Buttons/3/9-slices/9.png</key>
            <key type="filename">dist/UI/Buttons/4/9-slices/1.png</key>
            <key type="filename">dist/UI/Buttons/4/9-slices/2.png</key>
            <key type="filename">dist/UI/Buttons/4/9-slices/3.png</key>
            <key type="filename">dist/UI/Buttons/4/9-slices/4.png</key>
            <key type="filename">dist/UI/Buttons/4/9-slices/5.png</key>
            <key type="filename">dist/UI/Buttons/4/9-slices/6.png</key>
            <key type="filename">dist/UI/Buttons/4/9-slices/7.png</key>
            <key type="filename">dist/UI/Buttons/4/9-slices/8.png</key>
            <key type="filename">dist/UI/Buttons/4/9-slices/9.png</key>
            <key type="filename">dist/UI/Crossairs/1.png</key>
            <key type="filename">dist/UI/Crossairs/2.png</key>
            <key type="filename">dist/UI/Crossairs/3.png</key>
            <key type="filename">dist/UI/Crossairs/4.png</key>
            <key type="filename">dist/UI/Crossairs/5.png</key>
            <key type="filename">dist/UI/Crossairs/6.png</key>
            <key type="filename">dist/UI/Crossairs/7.png</key>
            <key type="filename">dist/UI/Crossairs/8.png</key>
            <key type="filename">dist/UI/Cursors/1.png</key>
            <key type="filename">dist/UI/Cursors/2.png</key>
            <key type="filename">dist/UI/Cursors/3.png</key>
            <key type="filename">dist/UI/Cursors/4.png</key>
            <key type="filename">dist/UI/Cursors/5.png</key>
            <key type="filename">dist/UI/Cursors/6.png</key>
            <key type="filename">dist/UI/Dots/1.png</key>
            <key type="filename">dist/UI/Dots/2.png</key>
            <key type="filename">dist/UI/Dots/3.png</key>
            <key type="filename">dist/UI/Dots/4.png</key>
            <key type="filename">dist/UI/Dots/5.png</key>
            <key type="filename">dist/UI/Dots/6.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerBL/9-slices/1.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerBL/9-slices/2.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerBL/9-slices/3.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerBL/9-slices/4.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerBL/9-slices/5.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerBL/9-slices/6.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerBL/9-slices/7.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerBL/9-slices/8.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerBL/9-slices/9.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerBR/9-slices/1.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerBR/9-slices/2.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerBR/9-slices/3.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerBR/9-slices/4.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerBR/9-slices/5.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerBR/9-slices/6.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerBR/9-slices/7.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerBR/9-slices/8.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerBR/9-slices/9.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerTL/9-slices/1.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerTL/9-slices/2.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerTL/9-slices/3.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerTL/9-slices/4.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerTL/9-slices/5.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerTL/9-slices/6.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerTL/9-slices/7.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerTL/9-slices/8.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerTL/9-slices/9.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerTR/9-slices/1.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerTR/9-slices/2.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerTR/9-slices/3.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerTR/9-slices/4.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerTR/9-slices/5.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerTR/9-slices/6.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerTR/9-slices/7.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerTR/9-slices/8.png</key>
            <key type="filename">dist/UI/GlassPanel-cornerTR/9-slices/9.png</key>
            <key type="filename">dist/UI/GlassPanel-corners/9-slices/1.png</key>
            <key type="filename">dist/UI/GlassPanel-corners/9-slices/2.png</key>
            <key type="filename">dist/UI/GlassPanel-corners/9-slices/3.png</key>
            <key type="filename">dist/UI/GlassPanel-corners/9-slices/4.png</key>
            <key type="filename">dist/UI/GlassPanel-corners/9-slices/5.png</key>
            <key type="filename">dist/UI/GlassPanel-corners/9-slices/6.png</key>
            <key type="filename">dist/UI/GlassPanel-corners/9-slices/7.png</key>
            <key type="filename">dist/UI/GlassPanel-corners/9-slices/8.png</key>
            <key type="filename">dist/UI/GlassPanel-corners/9-slices/9.png</key>
            <key type="filename">dist/UI/GlassPanel-empty/9-slices/1.png</key>
            <key type="filename">dist/UI/GlassPanel-empty/9-slices/2.png</key>
            <key type="filename">dist/UI/GlassPanel-empty/9-slices/3.png</key>
            <key type="filename">dist/UI/GlassPanel-empty/9-slices/4.png</key>
            <key type="filename">dist/UI/GlassPanel-empty/9-slices/5.png</key>
            <key type="filename">dist/UI/GlassPanel-empty/9-slices/6.png</key>
            <key type="filename">dist/UI/GlassPanel-empty/9-slices/7.png</key>
            <key type="filename">dist/UI/GlassPanel-empty/9-slices/8.png</key>
            <key type="filename">dist/UI/GlassPanel-empty/9-slices/9.png</key>
            <key type="filename">dist/UI/GlassPanel-projection/9-slices/1.png</key>
            <key type="filename">dist/UI/GlassPanel-projection/9-slices/2.png</key>
            <key type="filename">dist/UI/GlassPanel-projection/9-slices/3.png</key>
            <key type="filename">dist/UI/GlassPanel-projection/9-slices/4.png</key>
            <key type="filename">dist/UI/GlassPanel-projection/9-slices/5.png</key>
            <key type="filename">dist/UI/GlassPanel-projection/9-slices/6.png</key>
            <key type="filename">dist/UI/GlassPanel-projection/9-slices/7.png</key>
            <key type="filename">dist/UI/GlassPanel-projection/9-slices/8.png</key>
            <key type="filename">dist/UI/GlassPanel-projection/9-slices/9.png</key>
            <key type="filename">dist/UI/GlassPanel/9-slices/1.png</key>
            <key type="filename">dist/UI/GlassPanel/9-slices/2.png</key>
            <key type="filename">dist/UI/GlassPanel/9-slices/3.png</key>
            <key type="filename">dist/UI/GlassPanel/9-slices/4.png</key>
            <key type="filename">dist/UI/GlassPanel/9-slices/5.png</key>
            <key type="filename">dist/UI/GlassPanel/9-slices/6.png</key>
            <key type="filename">dist/UI/GlassPanel/9-slices/7.png</key>
            <key type="filename">dist/UI/GlassPanel/9-slices/8.png</key>
            <key type="filename">dist/UI/GlassPanel/9-slices/9.png</key>
            <key type="filename">dist/UI/Icons/ChampionCup.png</key>
            <key type="filename">dist/UI/Icons/FB/1.png</key>
            <key type="filename">dist/UI/Icons/Hearts/1.png</key>
            <key type="filename">dist/UI/Icons/Hearts/2.png</key>
            <key type="filename">dist/UI/Icons/Hearts/3.png</key>
            <key type="filename">dist/UI/Icons/Hearts/4.png</key>
            <key type="filename">dist/UI/Icons/JoyStickRight.png</key>
            <key type="filename">dist/UI/Icons/Lock.png</key>
            <key type="filename">dist/UI/Icons/Stars/1.png</key>
            <key type="filename">dist/UI/Icons/Stars/2.png</key>
            <key type="filename">dist/UI/Icons/Stars/3.png</key>
            <key type="filename">dist/UI/Icons/Stars/4.png</key>
            <key type="filename">dist/UI/Icons/Stars/5.png</key>
            <key type="filename">dist/UI/Icons/Stars/6.png</key>
            <key type="filename">dist/UI/Icons/Stars/7.png</key>
            <key type="filename">dist/UI/Icons/Stars/8.png</key>
            <key type="filename">dist/UI/Icons/Stars/9.png</key>
            <key type="filename">dist/UI/Icons/ThreePlayers.png</key>
            <key type="filename">dist/UI/Icons/TriangleRight.png</key>
            <key type="filename">dist/UI/Icons/Unlock.png</key>
            <key type="filename">dist/UI/Icons/X/1.png</key>
            <key type="filename">dist/UI/Icons/enter.png</key>
            <key type="filename">dist/UI/Icons/leaderBoard.png</key>
            <key type="filename">dist/UI/Icons/oneUser.png</key>
            <key type="filename">dist/UI/Icons/pause.png</key>
            <key type="filename">dist/UI/Icons/rotate.png</key>
            <key type="filename">dist/UI/KeyPanel/9-slices/1.png</key>
            <key type="filename">dist/UI/KeyPanel/9-slices/2.png</key>
            <key type="filename">dist/UI/KeyPanel/9-slices/3.png</key>
            <key type="filename">dist/UI/KeyPanel/9-slices/4.png</key>
            <key type="filename">dist/UI/KeyPanel/9-slices/5.png</key>
            <key type="filename">dist/UI/KeyPanel/9-slices/6.png</key>
            <key type="filename">dist/UI/KeyPanel/9-slices/7.png</key>
            <key type="filename">dist/UI/KeyPanel/9-slices/8.png</key>
            <key type="filename">dist/UI/KeyPanel/9-slices/9.png</key>
            <key type="filename">dist/UI/MetalBottomPanel/9-slices/1.png</key>
            <key type="filename">dist/UI/MetalBottomPanel/9-slices/2.png</key>
            <key type="filename">dist/UI/MetalBottomPanel/9-slices/3.png</key>
            <key type="filename">dist/UI/MetalBottomPanel/9-slices/4.png</key>
            <key type="filename">dist/UI/MetalBottomPanel/9-slices/5.png</key>
            <key type="filename">dist/UI/MetalBottomPanel/9-slices/6.png</key>
            <key type="filename">dist/UI/MetalBottomPanel/9-slices/7.png</key>
            <key type="filename">dist/UI/MetalBottomPanel/9-slices/8.png</key>
            <key type="filename">dist/UI/MetalBottomPanel/9-slices/9.png</key>
            <key type="filename">dist/UI/MetalPanel-plate/9-slices/1.png</key>
            <key type="filename">dist/UI/MetalPanel-plate/9-slices/2.png</key>
            <key type="filename">dist/UI/MetalPanel-plate/9-slices/3.png</key>
            <key type="filename">dist/UI/MetalPanel-plate/9-slices/4.png</key>
            <key type="filename">dist/UI/MetalPanel-plate/9-slices/5.png</key>
            <key type="filename">dist/UI/MetalPanel-plate/9-slices/6.png</key>
            <key type="filename">dist/UI/MetalPanel-plate/9-slices/7.png</key>
            <key type="filename">dist/UI/MetalPanel-plate/9-slices/8.png</key>
            <key type="filename">dist/UI/MetalPanel-plate/9-slices/9.png</key>
            <key type="filename">dist/UI/MetalPanel/9-slices/1.png</key>
            <key type="filename">dist/UI/MetalPanel/9-slices/2.png</key>
            <key type="filename">dist/UI/MetalPanel/9-slices/3.png</key>
            <key type="filename">dist/UI/MetalPanel/9-slices/4.png</key>
            <key type="filename">dist/UI/MetalPanel/9-slices/5.png</key>
            <key type="filename">dist/UI/MetalPanel/9-slices/6.png</key>
            <key type="filename">dist/UI/MetalPanel/9-slices/7.png</key>
            <key type="filename">dist/UI/MetalPanel/9-slices/8.png</key>
            <key type="filename">dist/UI/MetalPanel/9-slices/9.png</key>
            <key type="filename">dist/UI/Numeral/0.png</key>
            <key type="filename">dist/UI/Numeral/1.png</key>
            <key type="filename">dist/UI/Numeral/2.png</key>
            <key type="filename">dist/UI/Numeral/3.png</key>
            <key type="filename">dist/UI/Numeral/4.png</key>
            <key type="filename">dist/UI/Numeral/5.png</key>
            <key type="filename">dist/UI/Numeral/6.png</key>
            <key type="filename">dist/UI/Numeral/7.png</key>
            <key type="filename">dist/UI/Numeral/8.png</key>
            <key type="filename">dist/UI/Numeral/9.png</key>
            <key type="filename">dist/UI/Numeral/x.png</key>
            <key type="filename">dist/UI/Panel-white/9-slices/1.png</key>
            <key type="filename">dist/UI/Panel-white/9-slices/2.png</key>
            <key type="filename">dist/UI/Panel-white/9-slices/3.png</key>
            <key type="filename">dist/UI/Panel-white/9-slices/4.png</key>
            <key type="filename">dist/UI/Panel-white/9-slices/5.png</key>
            <key type="filename">dist/UI/Panel-white/9-slices/6.png</key>
            <key type="filename">dist/UI/Panel-white/9-slices/7.png</key>
            <key type="filename">dist/UI/Panel-white/9-slices/8.png</key>
            <key type="filename">dist/UI/Panel-white/9-slices/9.png</key>
            <key type="filename">dist/UI/Parts/glassPanel_tab_1.png</key>
            <key type="filename">dist/UI/Parts/glassPanel_tab_2.png</key>
            <key type="filename">dist/UI/Parts/glassPanel_tab_3.png</key>
            <key type="filename">dist/UI/Parts/glassPanel_tab_4.png</key>
            <key type="filename">dist/UI/Parts/glassPanel_tab_5.png</key>
            <key type="filename">dist/UI/Parts/glassPanel_tab_6.png</key>
            <key type="filename">dist/UI/Parts/glassPanel_tab_7.png</key>
            <key type="filename">dist/UI/Parts/glassPanel_tab_8.png</key>
            <key type="filename">dist/UI/Parts/glassPanel_tab_9.png</key>
            <key type="filename">dist/UI/Parts/glassPanel_tabbar_1.png</key>
            <key type="filename">dist/UI/Parts/glassPanel_tabbar_2.png</key>
            <key type="filename">dist/UI/Parts/glassPanel_tabbar_3.png</key>
            <key type="filename">dist/UI/Player-lifes/1.png</key>
            <key type="filename">dist/UI/Player-lifes/10.png</key>
            <key type="filename">dist/UI/Player-lifes/11.png</key>
            <key type="filename">dist/UI/Player-lifes/12.png</key>
            <key type="filename">dist/UI/Player-lifes/2.png</key>
            <key type="filename">dist/UI/Player-lifes/3.png</key>
            <key type="filename">dist/UI/Player-lifes/4.png</key>
            <key type="filename">dist/UI/Player-lifes/5.png</key>
            <key type="filename">dist/UI/Player-lifes/6.png</key>
            <key type="filename">dist/UI/Player-lifes/7.png</key>
            <key type="filename">dist/UI/Player-lifes/8.png</key>
            <key type="filename">dist/UI/Player-lifes/9.png</key>
            <key type="filename">dist/UI/Squares/1/9-slices/1.png</key>
            <key type="filename">dist/UI/Squares/1/9-slices/2.png</key>
            <key type="filename">dist/UI/Squares/1/9-slices/3.png</key>
            <key type="filename">dist/UI/Squares/1/9-slices/4.png</key>
            <key type="filename">dist/UI/Squares/1/9-slices/5.png</key>
            <key type="filename">dist/UI/Squares/1/9-slices/6.png</key>
            <key type="filename">dist/UI/Squares/1/9-slices/7.png</key>
            <key type="filename">dist/UI/Squares/1/9-slices/8.png</key>
            <key type="filename">dist/UI/Squares/1/9-slices/9.png</key>
            <key type="filename">dist/UI/Squares/2/9-slices/1.png</key>
            <key type="filename">dist/UI/Squares/2/9-slices/2.png</key>
            <key type="filename">dist/UI/Squares/2/9-slices/3.png</key>
            <key type="filename">dist/UI/Squares/2/9-slices/4.png</key>
            <key type="filename">dist/UI/Squares/2/9-slices/5.png</key>
            <key type="filename">dist/UI/Squares/2/9-slices/6.png</key>
            <key type="filename">dist/UI/Squares/2/9-slices/7.png</key>
            <key type="filename">dist/UI/Squares/2/9-slices/8.png</key>
            <key type="filename">dist/UI/Squares/2/9-slices/9.png</key>
            <key type="filename">dist/UI/Squares/3/9-slices/1.png</key>
            <key type="filename">dist/UI/Squares/3/9-slices/2.png</key>
            <key type="filename">dist/UI/Squares/3/9-slices/3.png</key>
            <key type="filename">dist/UI/Squares/3/9-slices/4.png</key>
            <key type="filename">dist/UI/Squares/3/9-slices/5.png</key>
            <key type="filename">dist/UI/Squares/3/9-slices/6.png</key>
            <key type="filename">dist/UI/Squares/3/9-slices/7.png</key>
            <key type="filename">dist/UI/Squares/3/9-slices/8.png</key>
            <key type="filename">dist/UI/Squares/3/9-slices/9.png</key>
            <key type="filename">dist/UI/Squares/4/9-slices/1.png</key>
            <key type="filename">dist/UI/Squares/4/9-slices/2.png</key>
            <key type="filename">dist/UI/Squares/4/9-slices/3.png</key>
            <key type="filename">dist/UI/Squares/4/9-slices/4.png</key>
            <key type="filename">dist/UI/Squares/4/9-slices/5.png</key>
            <key type="filename">dist/UI/Squares/4/9-slices/6.png</key>
            <key type="filename">dist/UI/Squares/4/9-slices/7.png</key>
            <key type="filename">dist/UI/Squares/4/9-slices/8.png</key>
            <key type="filename">dist/UI/Squares/4/9-slices/9.png</key>
            <key type="filename">dist/UI/Squares/5/9-slices/1.png</key>
            <key type="filename">dist/UI/Squares/5/9-slices/2.png</key>
            <key type="filename">dist/UI/Squares/5/9-slices/3.png</key>
            <key type="filename">dist/UI/Squares/5/9-slices/4.png</key>
            <key type="filename">dist/UI/Squares/5/9-slices/5.png</key>
            <key type="filename">dist/UI/Squares/5/9-slices/6.png</key>
            <key type="filename">dist/UI/Squares/5/9-slices/7.png</key>
            <key type="filename">dist/UI/Squares/5/9-slices/8.png</key>
            <key type="filename">dist/UI/Squares/5/9-slices/9.png</key>
            <key type="filename">dist/UI/Squares/6/9-slices/1.png</key>
            <key type="filename">dist/UI/Squares/6/9-slices/2.png</key>
            <key type="filename">dist/UI/Squares/6/9-slices/3.png</key>
            <key type="filename">dist/UI/Squares/6/9-slices/4.png</key>
            <key type="filename">dist/UI/Squares/6/9-slices/5.png</key>
            <key type="filename">dist/UI/Squares/6/9-slices/6.png</key>
            <key type="filename">dist/UI/Squares/6/9-slices/7.png</key>
            <key type="filename">dist/UI/Squares/6/9-slices/8.png</key>
            <key type="filename">dist/UI/Squares/6/9-slices/9.png</key>
            <key type="filename">dist/androidIcon.png</key>
            <key type="filename">dist/androidIcon@2x.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>dist</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
    </struct>
</data>
