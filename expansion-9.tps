<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.2.3</string>
        <key>fileName</key>
        <string>/Volumes/CLALASD/art/spaceshooter/expension-9.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string>@4x</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.75</double>
                <key>extension</key>
                <string>@3x</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.5</double>
                <key>extension</key>
                <string>@2x</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.25</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>corona-imagesheet</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>lua</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Project/shooting/sprites/expension-9{v}.lua</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <true/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">expension-9/Logo.png</key>
            <key type="filename">expension-9/UI/Bar-corners/1/3-slices/1.png</key>
            <key type="filename">expension-9/UI/Bar-corners/1/3-slices/2.png</key>
            <key type="filename">expension-9/UI/Bar-corners/1/3-slices/3.png</key>
            <key type="filename">expension-9/UI/Bar-corners/2/3-slices/1.png</key>
            <key type="filename">expension-9/UI/Bar-corners/2/3-slices/2.png</key>
            <key type="filename">expension-9/UI/Bar-corners/2/3-slices/3.png</key>
            <key type="filename">expension-9/UI/Bar-corners/3/3-slices/1.png</key>
            <key type="filename">expension-9/UI/Bar-corners/3/3-slices/2.png</key>
            <key type="filename">expension-9/UI/Bar-corners/3/3-slices/3.png</key>
            <key type="filename">expension-9/UI/Bar-corners/4/3-slices/1.png</key>
            <key type="filename">expension-9/UI/Bar-corners/4/3-slices/2.png</key>
            <key type="filename">expension-9/UI/Bar-corners/4/3-slices/3.png</key>
            <key type="filename">expension-9/UI/Bars/1/9-slices/1.png</key>
            <key type="filename">expension-9/UI/Bars/1/9-slices/2.png</key>
            <key type="filename">expension-9/UI/Bars/1/9-slices/3.png</key>
            <key type="filename">expension-9/UI/Bars/1/9-slices/4.png</key>
            <key type="filename">expension-9/UI/Bars/1/9-slices/5.png</key>
            <key type="filename">expension-9/UI/Bars/1/9-slices/6.png</key>
            <key type="filename">expension-9/UI/Bars/1/9-slices/7.png</key>
            <key type="filename">expension-9/UI/Bars/1/9-slices/8.png</key>
            <key type="filename">expension-9/UI/Bars/1/9-slices/9.png</key>
            <key type="filename">expension-9/UI/Bars/2/9-slices/1.png</key>
            <key type="filename">expension-9/UI/Bars/2/9-slices/2.png</key>
            <key type="filename">expension-9/UI/Bars/2/9-slices/3.png</key>
            <key type="filename">expension-9/UI/Bars/2/9-slices/4.png</key>
            <key type="filename">expension-9/UI/Bars/2/9-slices/5.png</key>
            <key type="filename">expension-9/UI/Bars/2/9-slices/6.png</key>
            <key type="filename">expension-9/UI/Bars/2/9-slices/7.png</key>
            <key type="filename">expension-9/UI/Bars/2/9-slices/8.png</key>
            <key type="filename">expension-9/UI/Bars/2/9-slices/9.png</key>
            <key type="filename">expension-9/UI/Bars/3/9-slices/1.png</key>
            <key type="filename">expension-9/UI/Bars/3/9-slices/2.png</key>
            <key type="filename">expension-9/UI/Bars/3/9-slices/3.png</key>
            <key type="filename">expension-9/UI/Bars/3/9-slices/4.png</key>
            <key type="filename">expension-9/UI/Bars/3/9-slices/5.png</key>
            <key type="filename">expension-9/UI/Bars/3/9-slices/6.png</key>
            <key type="filename">expension-9/UI/Bars/3/9-slices/7.png</key>
            <key type="filename">expension-9/UI/Bars/3/9-slices/8.png</key>
            <key type="filename">expension-9/UI/Bars/3/9-slices/9.png</key>
            <key type="filename">expension-9/UI/Bars/4/9-slices/1.png</key>
            <key type="filename">expension-9/UI/Bars/4/9-slices/2.png</key>
            <key type="filename">expension-9/UI/Bars/4/9-slices/3.png</key>
            <key type="filename">expension-9/UI/Bars/4/9-slices/4.png</key>
            <key type="filename">expension-9/UI/Bars/4/9-slices/5.png</key>
            <key type="filename">expension-9/UI/Bars/4/9-slices/6.png</key>
            <key type="filename">expension-9/UI/Bars/4/9-slices/7.png</key>
            <key type="filename">expension-9/UI/Bars/4/9-slices/8.png</key>
            <key type="filename">expension-9/UI/Bars/4/9-slices/9.png</key>
            <key type="filename">expension-9/UI/Buttons/1/9-slices/1.png</key>
            <key type="filename">expension-9/UI/Buttons/1/9-slices/2.png</key>
            <key type="filename">expension-9/UI/Buttons/1/9-slices/3.png</key>
            <key type="filename">expension-9/UI/Buttons/1/9-slices/4.png</key>
            <key type="filename">expension-9/UI/Buttons/1/9-slices/5.png</key>
            <key type="filename">expension-9/UI/Buttons/1/9-slices/6.png</key>
            <key type="filename">expension-9/UI/Buttons/1/9-slices/7.png</key>
            <key type="filename">expension-9/UI/Buttons/1/9-slices/8.png</key>
            <key type="filename">expension-9/UI/Buttons/1/9-slices/9.png</key>
            <key type="filename">expension-9/UI/Buttons/2/9-slices/1.png</key>
            <key type="filename">expension-9/UI/Buttons/2/9-slices/2.png</key>
            <key type="filename">expension-9/UI/Buttons/2/9-slices/3.png</key>
            <key type="filename">expension-9/UI/Buttons/2/9-slices/4.png</key>
            <key type="filename">expension-9/UI/Buttons/2/9-slices/5.png</key>
            <key type="filename">expension-9/UI/Buttons/2/9-slices/6.png</key>
            <key type="filename">expension-9/UI/Buttons/2/9-slices/7.png</key>
            <key type="filename">expension-9/UI/Buttons/2/9-slices/8.png</key>
            <key type="filename">expension-9/UI/Buttons/2/9-slices/9.png</key>
            <key type="filename">expension-9/UI/Buttons/3/9-slices/1.png</key>
            <key type="filename">expension-9/UI/Buttons/3/9-slices/2.png</key>
            <key type="filename">expension-9/UI/Buttons/3/9-slices/3.png</key>
            <key type="filename">expension-9/UI/Buttons/3/9-slices/4.png</key>
            <key type="filename">expension-9/UI/Buttons/3/9-slices/5.png</key>
            <key type="filename">expension-9/UI/Buttons/3/9-slices/6.png</key>
            <key type="filename">expension-9/UI/Buttons/3/9-slices/7.png</key>
            <key type="filename">expension-9/UI/Buttons/3/9-slices/8.png</key>
            <key type="filename">expension-9/UI/Buttons/3/9-slices/9.png</key>
            <key type="filename">expension-9/UI/Buttons/4/9-slices/1.png</key>
            <key type="filename">expension-9/UI/Buttons/4/9-slices/2.png</key>
            <key type="filename">expension-9/UI/Buttons/4/9-slices/3.png</key>
            <key type="filename">expension-9/UI/Buttons/4/9-slices/4.png</key>
            <key type="filename">expension-9/UI/Buttons/4/9-slices/5.png</key>
            <key type="filename">expension-9/UI/Buttons/4/9-slices/6.png</key>
            <key type="filename">expension-9/UI/Buttons/4/9-slices/7.png</key>
            <key type="filename">expension-9/UI/Buttons/4/9-slices/8.png</key>
            <key type="filename">expension-9/UI/Buttons/4/9-slices/9.png</key>
            <key type="filename">expension-9/UI/Crossairs/1.png</key>
            <key type="filename">expension-9/UI/Crossairs/2.png</key>
            <key type="filename">expension-9/UI/Crossairs/3.png</key>
            <key type="filename">expension-9/UI/Crossairs/4.png</key>
            <key type="filename">expension-9/UI/Crossairs/5.png</key>
            <key type="filename">expension-9/UI/Crossairs/6.png</key>
            <key type="filename">expension-9/UI/Crossairs/7.png</key>
            <key type="filename">expension-9/UI/Crossairs/8.png</key>
            <key type="filename">expension-9/UI/Cursors/1.png</key>
            <key type="filename">expension-9/UI/Cursors/2.png</key>
            <key type="filename">expension-9/UI/Cursors/3.png</key>
            <key type="filename">expension-9/UI/Cursors/4.png</key>
            <key type="filename">expension-9/UI/Cursors/5.png</key>
            <key type="filename">expension-9/UI/Cursors/6.png</key>
            <key type="filename">expension-9/UI/Dots/1.png</key>
            <key type="filename">expension-9/UI/Dots/2.png</key>
            <key type="filename">expension-9/UI/Dots/3.png</key>
            <key type="filename">expension-9/UI/Dots/4.png</key>
            <key type="filename">expension-9/UI/Dots/5.png</key>
            <key type="filename">expension-9/UI/Dots/6.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerBL/9-slices/1.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerBL/9-slices/2.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerBL/9-slices/3.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerBL/9-slices/4.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerBL/9-slices/5.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerBL/9-slices/6.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerBL/9-slices/7.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerBL/9-slices/8.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerBL/9-slices/9.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerBR/9-slices/1.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerBR/9-slices/2.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerBR/9-slices/3.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerBR/9-slices/4.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerBR/9-slices/5.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerBR/9-slices/6.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerBR/9-slices/7.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerBR/9-slices/8.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerBR/9-slices/9.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerTL/9-slices/1.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerTL/9-slices/2.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerTL/9-slices/3.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerTL/9-slices/4.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerTL/9-slices/5.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerTL/9-slices/6.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerTL/9-slices/7.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerTL/9-slices/8.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerTL/9-slices/9.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerTR/9-slices/1.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerTR/9-slices/2.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerTR/9-slices/3.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerTR/9-slices/4.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerTR/9-slices/5.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerTR/9-slices/6.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerTR/9-slices/7.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerTR/9-slices/8.png</key>
            <key type="filename">expension-9/UI/GlassPanel-cornerTR/9-slices/9.png</key>
            <key type="filename">expension-9/UI/GlassPanel-corners/9-slices/1.png</key>
            <key type="filename">expension-9/UI/GlassPanel-corners/9-slices/2.png</key>
            <key type="filename">expension-9/UI/GlassPanel-corners/9-slices/3.png</key>
            <key type="filename">expension-9/UI/GlassPanel-corners/9-slices/4.png</key>
            <key type="filename">expension-9/UI/GlassPanel-corners/9-slices/5.png</key>
            <key type="filename">expension-9/UI/GlassPanel-corners/9-slices/6.png</key>
            <key type="filename">expension-9/UI/GlassPanel-corners/9-slices/7.png</key>
            <key type="filename">expension-9/UI/GlassPanel-corners/9-slices/8.png</key>
            <key type="filename">expension-9/UI/GlassPanel-corners/9-slices/9.png</key>
            <key type="filename">expension-9/UI/GlassPanel-empty/9-slices/1.png</key>
            <key type="filename">expension-9/UI/GlassPanel-empty/9-slices/2.png</key>
            <key type="filename">expension-9/UI/GlassPanel-empty/9-slices/3.png</key>
            <key type="filename">expension-9/UI/GlassPanel-empty/9-slices/4.png</key>
            <key type="filename">expension-9/UI/GlassPanel-empty/9-slices/5.png</key>
            <key type="filename">expension-9/UI/GlassPanel-empty/9-slices/6.png</key>
            <key type="filename">expension-9/UI/GlassPanel-empty/9-slices/7.png</key>
            <key type="filename">expension-9/UI/GlassPanel-empty/9-slices/8.png</key>
            <key type="filename">expension-9/UI/GlassPanel-empty/9-slices/9.png</key>
            <key type="filename">expension-9/UI/GlassPanel-projection/9-slices/1.png</key>
            <key type="filename">expension-9/UI/GlassPanel-projection/9-slices/2.png</key>
            <key type="filename">expension-9/UI/GlassPanel-projection/9-slices/3.png</key>
            <key type="filename">expension-9/UI/GlassPanel-projection/9-slices/4.png</key>
            <key type="filename">expension-9/UI/GlassPanel-projection/9-slices/5.png</key>
            <key type="filename">expension-9/UI/GlassPanel-projection/9-slices/6.png</key>
            <key type="filename">expension-9/UI/GlassPanel-projection/9-slices/7.png</key>
            <key type="filename">expension-9/UI/GlassPanel-projection/9-slices/8.png</key>
            <key type="filename">expension-9/UI/GlassPanel-projection/9-slices/9.png</key>
            <key type="filename">expension-9/UI/GlassPanel/9-slices/1.png</key>
            <key type="filename">expension-9/UI/GlassPanel/9-slices/2.png</key>
            <key type="filename">expension-9/UI/GlassPanel/9-slices/3.png</key>
            <key type="filename">expension-9/UI/GlassPanel/9-slices/4.png</key>
            <key type="filename">expension-9/UI/GlassPanel/9-slices/5.png</key>
            <key type="filename">expension-9/UI/GlassPanel/9-slices/6.png</key>
            <key type="filename">expension-9/UI/GlassPanel/9-slices/7.png</key>
            <key type="filename">expension-9/UI/GlassPanel/9-slices/8.png</key>
            <key type="filename">expension-9/UI/GlassPanel/9-slices/9.png</key>
            <key type="filename">expension-9/UI/Icons/ChampionCup.png</key>
            <key type="filename">expension-9/UI/Icons/FB/1.png</key>
            <key type="filename">expension-9/UI/Icons/Hearts/1.png</key>
            <key type="filename">expension-9/UI/Icons/Hearts/2.png</key>
            <key type="filename">expension-9/UI/Icons/Hearts/3.png</key>
            <key type="filename">expension-9/UI/Icons/Hearts/4.png</key>
            <key type="filename">expension-9/UI/Icons/JoyStickRight.png</key>
            <key type="filename">expension-9/UI/Icons/Lock.png</key>
            <key type="filename">expension-9/UI/Icons/Stars/1.png</key>
            <key type="filename">expension-9/UI/Icons/Stars/2.png</key>
            <key type="filename">expension-9/UI/Icons/Stars/3.png</key>
            <key type="filename">expension-9/UI/Icons/Stars/4.png</key>
            <key type="filename">expension-9/UI/Icons/Stars/5.png</key>
            <key type="filename">expension-9/UI/Icons/Stars/6.png</key>
            <key type="filename">expension-9/UI/Icons/Stars/7.png</key>
            <key type="filename">expension-9/UI/Icons/Stars/8.png</key>
            <key type="filename">expension-9/UI/Icons/Stars/9.png</key>
            <key type="filename">expension-9/UI/Icons/ThreePlayers.png</key>
            <key type="filename">expension-9/UI/Icons/TriangleRight.png</key>
            <key type="filename">expension-9/UI/Icons/Unlock.png</key>
            <key type="filename">expension-9/UI/Icons/X/1.png</key>
            <key type="filename">expension-9/UI/Icons/enter.png</key>
            <key type="filename">expension-9/UI/Icons/leaderBoard.png</key>
            <key type="filename">expension-9/UI/Icons/oneUser.png</key>
            <key type="filename">expension-9/UI/Icons/pause.png</key>
            <key type="filename">expension-9/UI/Icons/rotate.png</key>
            <key type="filename">expension-9/UI/KeyPanel/9-slices/1.png</key>
            <key type="filename">expension-9/UI/KeyPanel/9-slices/2.png</key>
            <key type="filename">expension-9/UI/KeyPanel/9-slices/3.png</key>
            <key type="filename">expension-9/UI/KeyPanel/9-slices/4.png</key>
            <key type="filename">expension-9/UI/KeyPanel/9-slices/5.png</key>
            <key type="filename">expension-9/UI/KeyPanel/9-slices/6.png</key>
            <key type="filename">expension-9/UI/KeyPanel/9-slices/7.png</key>
            <key type="filename">expension-9/UI/KeyPanel/9-slices/8.png</key>
            <key type="filename">expension-9/UI/KeyPanel/9-slices/9.png</key>
            <key type="filename">expension-9/UI/MetalBottomPanel/9-slices/1.png</key>
            <key type="filename">expension-9/UI/MetalBottomPanel/9-slices/2.png</key>
            <key type="filename">expension-9/UI/MetalBottomPanel/9-slices/3.png</key>
            <key type="filename">expension-9/UI/MetalBottomPanel/9-slices/4.png</key>
            <key type="filename">expension-9/UI/MetalBottomPanel/9-slices/5.png</key>
            <key type="filename">expension-9/UI/MetalBottomPanel/9-slices/6.png</key>
            <key type="filename">expension-9/UI/MetalBottomPanel/9-slices/7.png</key>
            <key type="filename">expension-9/UI/MetalBottomPanel/9-slices/8.png</key>
            <key type="filename">expension-9/UI/MetalBottomPanel/9-slices/9.png</key>
            <key type="filename">expension-9/UI/MetalPanel-plate/9-slices/1.png</key>
            <key type="filename">expension-9/UI/MetalPanel-plate/9-slices/2.png</key>
            <key type="filename">expension-9/UI/MetalPanel-plate/9-slices/3.png</key>
            <key type="filename">expension-9/UI/MetalPanel-plate/9-slices/4.png</key>
            <key type="filename">expension-9/UI/MetalPanel-plate/9-slices/5.png</key>
            <key type="filename">expension-9/UI/MetalPanel-plate/9-slices/6.png</key>
            <key type="filename">expension-9/UI/MetalPanel-plate/9-slices/7.png</key>
            <key type="filename">expension-9/UI/MetalPanel-plate/9-slices/8.png</key>
            <key type="filename">expension-9/UI/MetalPanel-plate/9-slices/9.png</key>
            <key type="filename">expension-9/UI/MetalPanel/9-slices/1.png</key>
            <key type="filename">expension-9/UI/MetalPanel/9-slices/2.png</key>
            <key type="filename">expension-9/UI/MetalPanel/9-slices/3.png</key>
            <key type="filename">expension-9/UI/MetalPanel/9-slices/4.png</key>
            <key type="filename">expension-9/UI/MetalPanel/9-slices/5.png</key>
            <key type="filename">expension-9/UI/MetalPanel/9-slices/6.png</key>
            <key type="filename">expension-9/UI/MetalPanel/9-slices/7.png</key>
            <key type="filename">expension-9/UI/MetalPanel/9-slices/8.png</key>
            <key type="filename">expension-9/UI/MetalPanel/9-slices/9.png</key>
            <key type="filename">expension-9/UI/Numeral/0.png</key>
            <key type="filename">expension-9/UI/Numeral/1.png</key>
            <key type="filename">expension-9/UI/Numeral/2.png</key>
            <key type="filename">expension-9/UI/Numeral/3.png</key>
            <key type="filename">expension-9/UI/Numeral/4.png</key>
            <key type="filename">expension-9/UI/Numeral/5.png</key>
            <key type="filename">expension-9/UI/Numeral/6.png</key>
            <key type="filename">expension-9/UI/Numeral/7.png</key>
            <key type="filename">expension-9/UI/Numeral/8.png</key>
            <key type="filename">expension-9/UI/Numeral/9.png</key>
            <key type="filename">expension-9/UI/Numeral/x.png</key>
            <key type="filename">expension-9/UI/Panel-white/9-slices/1.png</key>
            <key type="filename">expension-9/UI/Panel-white/9-slices/2.png</key>
            <key type="filename">expension-9/UI/Panel-white/9-slices/3.png</key>
            <key type="filename">expension-9/UI/Panel-white/9-slices/4.png</key>
            <key type="filename">expension-9/UI/Panel-white/9-slices/5.png</key>
            <key type="filename">expension-9/UI/Panel-white/9-slices/6.png</key>
            <key type="filename">expension-9/UI/Panel-white/9-slices/7.png</key>
            <key type="filename">expension-9/UI/Panel-white/9-slices/8.png</key>
            <key type="filename">expension-9/UI/Panel-white/9-slices/9.png</key>
            <key type="filename">expension-9/UI/Parts/glassPanel_tab_1.png</key>
            <key type="filename">expension-9/UI/Parts/glassPanel_tab_2.png</key>
            <key type="filename">expension-9/UI/Parts/glassPanel_tab_3.png</key>
            <key type="filename">expension-9/UI/Parts/glassPanel_tab_4.png</key>
            <key type="filename">expension-9/UI/Parts/glassPanel_tab_5.png</key>
            <key type="filename">expension-9/UI/Parts/glassPanel_tab_6.png</key>
            <key type="filename">expension-9/UI/Parts/glassPanel_tab_7.png</key>
            <key type="filename">expension-9/UI/Parts/glassPanel_tab_8.png</key>
            <key type="filename">expension-9/UI/Parts/glassPanel_tab_9.png</key>
            <key type="filename">expension-9/UI/Parts/glassPanel_tabbar_1.png</key>
            <key type="filename">expension-9/UI/Parts/glassPanel_tabbar_2.png</key>
            <key type="filename">expension-9/UI/Parts/glassPanel_tabbar_3.png</key>
            <key type="filename">expension-9/UI/Player-lifes/1.png</key>
            <key type="filename">expension-9/UI/Player-lifes/10.png</key>
            <key type="filename">expension-9/UI/Player-lifes/11.png</key>
            <key type="filename">expension-9/UI/Player-lifes/12.png</key>
            <key type="filename">expension-9/UI/Player-lifes/2.png</key>
            <key type="filename">expension-9/UI/Player-lifes/3.png</key>
            <key type="filename">expension-9/UI/Player-lifes/4.png</key>
            <key type="filename">expension-9/UI/Player-lifes/5.png</key>
            <key type="filename">expension-9/UI/Player-lifes/6.png</key>
            <key type="filename">expension-9/UI/Player-lifes/7.png</key>
            <key type="filename">expension-9/UI/Player-lifes/8.png</key>
            <key type="filename">expension-9/UI/Player-lifes/9.png</key>
            <key type="filename">expension-9/UI/Squares/1/9-slices/1.png</key>
            <key type="filename">expension-9/UI/Squares/1/9-slices/2.png</key>
            <key type="filename">expension-9/UI/Squares/1/9-slices/3.png</key>
            <key type="filename">expension-9/UI/Squares/1/9-slices/4.png</key>
            <key type="filename">expension-9/UI/Squares/1/9-slices/5.png</key>
            <key type="filename">expension-9/UI/Squares/1/9-slices/6.png</key>
            <key type="filename">expension-9/UI/Squares/1/9-slices/7.png</key>
            <key type="filename">expension-9/UI/Squares/1/9-slices/8.png</key>
            <key type="filename">expension-9/UI/Squares/1/9-slices/9.png</key>
            <key type="filename">expension-9/UI/Squares/2/9-slices/1.png</key>
            <key type="filename">expension-9/UI/Squares/2/9-slices/2.png</key>
            <key type="filename">expension-9/UI/Squares/2/9-slices/3.png</key>
            <key type="filename">expension-9/UI/Squares/2/9-slices/4.png</key>
            <key type="filename">expension-9/UI/Squares/2/9-slices/5.png</key>
            <key type="filename">expension-9/UI/Squares/2/9-slices/6.png</key>
            <key type="filename">expension-9/UI/Squares/2/9-slices/7.png</key>
            <key type="filename">expension-9/UI/Squares/2/9-slices/8.png</key>
            <key type="filename">expension-9/UI/Squares/2/9-slices/9.png</key>
            <key type="filename">expension-9/UI/Squares/3/9-slices/1.png</key>
            <key type="filename">expension-9/UI/Squares/3/9-slices/2.png</key>
            <key type="filename">expension-9/UI/Squares/3/9-slices/3.png</key>
            <key type="filename">expension-9/UI/Squares/3/9-slices/4.png</key>
            <key type="filename">expension-9/UI/Squares/3/9-slices/5.png</key>
            <key type="filename">expension-9/UI/Squares/3/9-slices/6.png</key>
            <key type="filename">expension-9/UI/Squares/3/9-slices/7.png</key>
            <key type="filename">expension-9/UI/Squares/3/9-slices/8.png</key>
            <key type="filename">expension-9/UI/Squares/3/9-slices/9.png</key>
            <key type="filename">expension-9/UI/Squares/4/9-slices/1.png</key>
            <key type="filename">expension-9/UI/Squares/4/9-slices/2.png</key>
            <key type="filename">expension-9/UI/Squares/4/9-slices/3.png</key>
            <key type="filename">expension-9/UI/Squares/4/9-slices/4.png</key>
            <key type="filename">expension-9/UI/Squares/4/9-slices/5.png</key>
            <key type="filename">expension-9/UI/Squares/4/9-slices/6.png</key>
            <key type="filename">expension-9/UI/Squares/4/9-slices/7.png</key>
            <key type="filename">expension-9/UI/Squares/4/9-slices/8.png</key>
            <key type="filename">expension-9/UI/Squares/4/9-slices/9.png</key>
            <key type="filename">expension-9/UI/Squares/5/9-slices/1.png</key>
            <key type="filename">expension-9/UI/Squares/5/9-slices/2.png</key>
            <key type="filename">expension-9/UI/Squares/5/9-slices/3.png</key>
            <key type="filename">expension-9/UI/Squares/5/9-slices/4.png</key>
            <key type="filename">expension-9/UI/Squares/5/9-slices/5.png</key>
            <key type="filename">expension-9/UI/Squares/5/9-slices/6.png</key>
            <key type="filename">expension-9/UI/Squares/5/9-slices/7.png</key>
            <key type="filename">expension-9/UI/Squares/5/9-slices/8.png</key>
            <key type="filename">expension-9/UI/Squares/5/9-slices/9.png</key>
            <key type="filename">expension-9/UI/Squares/6/9-slices/1.png</key>
            <key type="filename">expension-9/UI/Squares/6/9-slices/2.png</key>
            <key type="filename">expension-9/UI/Squares/6/9-slices/3.png</key>
            <key type="filename">expension-9/UI/Squares/6/9-slices/4.png</key>
            <key type="filename">expension-9/UI/Squares/6/9-slices/5.png</key>
            <key type="filename">expension-9/UI/Squares/6/9-slices/6.png</key>
            <key type="filename">expension-9/UI/Squares/6/9-slices/7.png</key>
            <key type="filename">expension-9/UI/Squares/6/9-slices/8.png</key>
            <key type="filename">expension-9/UI/Squares/6/9-slices/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>expension-9</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
    </struct>
</data>
