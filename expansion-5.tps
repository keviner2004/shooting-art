<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.2.3</string>
        <key>fileName</key>
        <string>/Volumes/CLALASD/art/spaceshooter/expansion-5.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string>@4x</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.75</double>
                <key>extension</key>
                <string>@3x</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.5</double>
                <key>extension</key>
                <string>@2x</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.25</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>corona-imagesheet</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>lua</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Project/shooting/sprites/expansion-5{v}.lua</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <true/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">expansion-5/Rockets/Parts/1.png</key>
            <key type="filename">expansion-5/Rockets/Parts/10.png</key>
            <key type="filename">expansion-5/Rockets/Parts/11.png</key>
            <key type="filename">expansion-5/Rockets/Parts/12.png</key>
            <key type="filename">expansion-5/Rockets/Parts/13.png</key>
            <key type="filename">expansion-5/Rockets/Parts/14.png</key>
            <key type="filename">expansion-5/Rockets/Parts/15.png</key>
            <key type="filename">expansion-5/Rockets/Parts/16.png</key>
            <key type="filename">expansion-5/Rockets/Parts/17.png</key>
            <key type="filename">expansion-5/Rockets/Parts/18.png</key>
            <key type="filename">expansion-5/Rockets/Parts/19.png</key>
            <key type="filename">expansion-5/Rockets/Parts/2.png</key>
            <key type="filename">expansion-5/Rockets/Parts/20.png</key>
            <key type="filename">expansion-5/Rockets/Parts/21.png</key>
            <key type="filename">expansion-5/Rockets/Parts/22.png</key>
            <key type="filename">expansion-5/Rockets/Parts/23.png</key>
            <key type="filename">expansion-5/Rockets/Parts/24.png</key>
            <key type="filename">expansion-5/Rockets/Parts/25.png</key>
            <key type="filename">expansion-5/Rockets/Parts/26.png</key>
            <key type="filename">expansion-5/Rockets/Parts/27.png</key>
            <key type="filename">expansion-5/Rockets/Parts/28.png</key>
            <key type="filename">expansion-5/Rockets/Parts/29.png</key>
            <key type="filename">expansion-5/Rockets/Parts/3.png</key>
            <key type="filename">expansion-5/Rockets/Parts/30.png</key>
            <key type="filename">expansion-5/Rockets/Parts/31.png</key>
            <key type="filename">expansion-5/Rockets/Parts/4.png</key>
            <key type="filename">expansion-5/Rockets/Parts/5.png</key>
            <key type="filename">expansion-5/Rockets/Parts/6.png</key>
            <key type="filename">expansion-5/Rockets/Parts/7.png</key>
            <key type="filename">expansion-5/Rockets/Parts/8.png</key>
            <key type="filename">expansion-5/Rockets/Parts/9.png</key>
            <key type="filename">expansion-5/Rockets/SpaceRockets/1.png</key>
            <key type="filename">expansion-5/Rockets/SpaceRockets/2.png</key>
            <key type="filename">expansion-5/Rockets/SpaceRockets/3 Copy.png</key>
            <key type="filename">expansion-5/Rockets/SpaceRockets/3.png</key>
            <key type="filename">expansion-5/Rockets/SpaceRockets/4.png</key>
            <key type="filename">expansion-5/Stations/1.png</key>
            <key type="filename">expansion-5/Stations/10.png</key>
            <key type="filename">expansion-5/Stations/11.png</key>
            <key type="filename">expansion-5/Stations/2.png</key>
            <key type="filename">expansion-5/Stations/3.png</key>
            <key type="filename">expansion-5/Stations/4.png</key>
            <key type="filename">expansion-5/Stations/5.png</key>
            <key type="filename">expansion-5/Stations/6.png</key>
            <key type="filename">expansion-5/Stations/7.png</key>
            <key type="filename">expansion-5/Stations/8.png</key>
            <key type="filename">expansion-5/Stations/9.png</key>
            <key type="filename">expansion-5/Stations/Parts/1.png</key>
            <key type="filename">expansion-5/Stations/Parts/10.png</key>
            <key type="filename">expansion-5/Stations/Parts/11.png</key>
            <key type="filename">expansion-5/Stations/Parts/12.png</key>
            <key type="filename">expansion-5/Stations/Parts/13.png</key>
            <key type="filename">expansion-5/Stations/Parts/14.png</key>
            <key type="filename">expansion-5/Stations/Parts/15.png</key>
            <key type="filename">expansion-5/Stations/Parts/16.png</key>
            <key type="filename">expansion-5/Stations/Parts/17.png</key>
            <key type="filename">expansion-5/Stations/Parts/18.png</key>
            <key type="filename">expansion-5/Stations/Parts/19.png</key>
            <key type="filename">expansion-5/Stations/Parts/2.png</key>
            <key type="filename">expansion-5/Stations/Parts/20.png</key>
            <key type="filename">expansion-5/Stations/Parts/21.png</key>
            <key type="filename">expansion-5/Stations/Parts/22.png</key>
            <key type="filename">expansion-5/Stations/Parts/23.png</key>
            <key type="filename">expansion-5/Stations/Parts/24.png</key>
            <key type="filename">expansion-5/Stations/Parts/3.png</key>
            <key type="filename">expansion-5/Stations/Parts/4.png</key>
            <key type="filename">expansion-5/Stations/Parts/5.png</key>
            <key type="filename">expansion-5/Stations/Parts/6.png</key>
            <key type="filename">expansion-5/Stations/Parts/7.png</key>
            <key type="filename">expansion-5/Stations/Parts/8.png</key>
            <key type="filename">expansion-5/Stations/Parts/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>expansion-5</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
    </struct>
</data>
