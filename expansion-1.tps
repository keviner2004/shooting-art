<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.2.3</string>
        <key>fileName</key>
        <string>/Volumes/CLALASD/art/spaceshooter/expension-1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string>@4x</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.75</double>
                <key>extension</key>
                <string>@3x</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.5</double>
                <key>extension</key>
                <string>@2x</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.25</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>corona-imagesheet</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>lua</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Project/shooting/sprites/expansion-1{v}.lua</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <true/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">expansion-1/Items/1.png</key>
            <key type="filename">expansion-1/Items/10.png</key>
            <key type="filename">expansion-1/Items/100.png</key>
            <key type="filename">expansion-1/Items/101.png</key>
            <key type="filename">expansion-1/Items/102.png</key>
            <key type="filename">expansion-1/Items/103.png</key>
            <key type="filename">expansion-1/Items/104.png</key>
            <key type="filename">expansion-1/Items/105.png</key>
            <key type="filename">expansion-1/Items/106.png</key>
            <key type="filename">expansion-1/Items/107.png</key>
            <key type="filename">expansion-1/Items/108.png</key>
            <key type="filename">expansion-1/Items/109.png</key>
            <key type="filename">expansion-1/Items/11.png</key>
            <key type="filename">expansion-1/Items/110.png</key>
            <key type="filename">expansion-1/Items/111.png</key>
            <key type="filename">expansion-1/Items/112.png</key>
            <key type="filename">expansion-1/Items/113.png</key>
            <key type="filename">expansion-1/Items/114.png</key>
            <key type="filename">expansion-1/Items/115.png</key>
            <key type="filename">expansion-1/Items/116.png</key>
            <key type="filename">expansion-1/Items/117.png</key>
            <key type="filename">expansion-1/Items/118.png</key>
            <key type="filename">expansion-1/Items/119.png</key>
            <key type="filename">expansion-1/Items/12.png</key>
            <key type="filename">expansion-1/Items/120.png</key>
            <key type="filename">expansion-1/Items/121.png</key>
            <key type="filename">expansion-1/Items/122.png</key>
            <key type="filename">expansion-1/Items/123.png</key>
            <key type="filename">expansion-1/Items/124.png</key>
            <key type="filename">expansion-1/Items/125.png</key>
            <key type="filename">expansion-1/Items/126.png</key>
            <key type="filename">expansion-1/Items/127.png</key>
            <key type="filename">expansion-1/Items/128.png</key>
            <key type="filename">expansion-1/Items/129.png</key>
            <key type="filename">expansion-1/Items/13.png</key>
            <key type="filename">expansion-1/Items/130.png</key>
            <key type="filename">expansion-1/Items/131.png</key>
            <key type="filename">expansion-1/Items/132.png</key>
            <key type="filename">expansion-1/Items/133.png</key>
            <key type="filename">expansion-1/Items/134.png</key>
            <key type="filename">expansion-1/Items/135.png</key>
            <key type="filename">expansion-1/Items/136.png</key>
            <key type="filename">expansion-1/Items/137.png</key>
            <key type="filename">expansion-1/Items/138.png</key>
            <key type="filename">expansion-1/Items/139.png</key>
            <key type="filename">expansion-1/Items/14.png</key>
            <key type="filename">expansion-1/Items/140.png</key>
            <key type="filename">expansion-1/Items/141.png</key>
            <key type="filename">expansion-1/Items/142.png</key>
            <key type="filename">expansion-1/Items/143.png</key>
            <key type="filename">expansion-1/Items/144.png</key>
            <key type="filename">expansion-1/Items/145.png</key>
            <key type="filename">expansion-1/Items/146.png</key>
            <key type="filename">expansion-1/Items/147.png</key>
            <key type="filename">expansion-1/Items/148.png</key>
            <key type="filename">expansion-1/Items/149.png</key>
            <key type="filename">expansion-1/Items/15.png</key>
            <key type="filename">expansion-1/Items/150.png</key>
            <key type="filename">expansion-1/Items/151.png</key>
            <key type="filename">expansion-1/Items/152.png</key>
            <key type="filename">expansion-1/Items/153.png</key>
            <key type="filename">expansion-1/Items/154.png</key>
            <key type="filename">expansion-1/Items/155.png</key>
            <key type="filename">expansion-1/Items/156.png</key>
            <key type="filename">expansion-1/Items/157.png</key>
            <key type="filename">expansion-1/Items/158.png</key>
            <key type="filename">expansion-1/Items/159.png</key>
            <key type="filename">expansion-1/Items/16.png</key>
            <key type="filename">expansion-1/Items/160.png</key>
            <key type="filename">expansion-1/Items/161.png</key>
            <key type="filename">expansion-1/Items/162.png</key>
            <key type="filename">expansion-1/Items/163.png</key>
            <key type="filename">expansion-1/Items/17.png</key>
            <key type="filename">expansion-1/Items/18.png</key>
            <key type="filename">expansion-1/Items/19.png</key>
            <key type="filename">expansion-1/Items/2.png</key>
            <key type="filename">expansion-1/Items/20.png</key>
            <key type="filename">expansion-1/Items/21.png</key>
            <key type="filename">expansion-1/Items/22.png</key>
            <key type="filename">expansion-1/Items/23.png</key>
            <key type="filename">expansion-1/Items/24.png</key>
            <key type="filename">expansion-1/Items/25.png</key>
            <key type="filename">expansion-1/Items/26.png</key>
            <key type="filename">expansion-1/Items/27.png</key>
            <key type="filename">expansion-1/Items/28.png</key>
            <key type="filename">expansion-1/Items/29.png</key>
            <key type="filename">expansion-1/Items/3.png</key>
            <key type="filename">expansion-1/Items/30.png</key>
            <key type="filename">expansion-1/Items/31.png</key>
            <key type="filename">expansion-1/Items/32.png</key>
            <key type="filename">expansion-1/Items/33.png</key>
            <key type="filename">expansion-1/Items/34.png</key>
            <key type="filename">expansion-1/Items/35.png</key>
            <key type="filename">expansion-1/Items/36.png</key>
            <key type="filename">expansion-1/Items/37.png</key>
            <key type="filename">expansion-1/Items/38.png</key>
            <key type="filename">expansion-1/Items/39.png</key>
            <key type="filename">expansion-1/Items/4.png</key>
            <key type="filename">expansion-1/Items/40.png</key>
            <key type="filename">expansion-1/Items/41.png</key>
            <key type="filename">expansion-1/Items/42.png</key>
            <key type="filename">expansion-1/Items/43.png</key>
            <key type="filename">expansion-1/Items/44.png</key>
            <key type="filename">expansion-1/Items/45.png</key>
            <key type="filename">expansion-1/Items/46.png</key>
            <key type="filename">expansion-1/Items/47.png</key>
            <key type="filename">expansion-1/Items/48.png</key>
            <key type="filename">expansion-1/Items/49.png</key>
            <key type="filename">expansion-1/Items/5.png</key>
            <key type="filename">expansion-1/Items/50.png</key>
            <key type="filename">expansion-1/Items/51.png</key>
            <key type="filename">expansion-1/Items/52.png</key>
            <key type="filename">expansion-1/Items/53.png</key>
            <key type="filename">expansion-1/Items/54.png</key>
            <key type="filename">expansion-1/Items/55.png</key>
            <key type="filename">expansion-1/Items/56.png</key>
            <key type="filename">expansion-1/Items/57.png</key>
            <key type="filename">expansion-1/Items/58.png</key>
            <key type="filename">expansion-1/Items/59.png</key>
            <key type="filename">expansion-1/Items/6.png</key>
            <key type="filename">expansion-1/Items/60.png</key>
            <key type="filename">expansion-1/Items/61.png</key>
            <key type="filename">expansion-1/Items/62.png</key>
            <key type="filename">expansion-1/Items/63.png</key>
            <key type="filename">expansion-1/Items/64.png</key>
            <key type="filename">expansion-1/Items/65.png</key>
            <key type="filename">expansion-1/Items/66.png</key>
            <key type="filename">expansion-1/Items/67.png</key>
            <key type="filename">expansion-1/Items/68.png</key>
            <key type="filename">expansion-1/Items/69.png</key>
            <key type="filename">expansion-1/Items/7.png</key>
            <key type="filename">expansion-1/Items/70.png</key>
            <key type="filename">expansion-1/Items/71.png</key>
            <key type="filename">expansion-1/Items/72.png</key>
            <key type="filename">expansion-1/Items/73.png</key>
            <key type="filename">expansion-1/Items/74.png</key>
            <key type="filename">expansion-1/Items/75.png</key>
            <key type="filename">expansion-1/Items/76.png</key>
            <key type="filename">expansion-1/Items/77.png</key>
            <key type="filename">expansion-1/Items/78.png</key>
            <key type="filename">expansion-1/Items/79.png</key>
            <key type="filename">expansion-1/Items/8.png</key>
            <key type="filename">expansion-1/Items/80.png</key>
            <key type="filename">expansion-1/Items/81.png</key>
            <key type="filename">expansion-1/Items/82.png</key>
            <key type="filename">expansion-1/Items/83.png</key>
            <key type="filename">expansion-1/Items/84.png</key>
            <key type="filename">expansion-1/Items/85.png</key>
            <key type="filename">expansion-1/Items/86.png</key>
            <key type="filename">expansion-1/Items/87.png</key>
            <key type="filename">expansion-1/Items/88.png</key>
            <key type="filename">expansion-1/Items/89.png</key>
            <key type="filename">expansion-1/Items/9.png</key>
            <key type="filename">expansion-1/Items/90.png</key>
            <key type="filename">expansion-1/Items/91.png</key>
            <key type="filename">expansion-1/Items/92.png</key>
            <key type="filename">expansion-1/Items/93.png</key>
            <key type="filename">expansion-1/Items/94.png</key>
            <key type="filename">expansion-1/Items/95.png</key>
            <key type="filename">expansion-1/Items/96.png</key>
            <key type="filename">expansion-1/Items/97.png</key>
            <key type="filename">expansion-1/Items/98.png</key>
            <key type="filename">expansion-1/Items/99.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>expansion-1</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
    </struct>
</data>
