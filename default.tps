<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.2.3</string>
        <key>fileName</key>
        <string>/Volumes/CLALASD/art/spaceshooter/default.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string>@4x</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.75</double>
                <key>extension</key>
                <string>@3x</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.5</double>
                <key>extension</key>
                <string>@2x</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.25</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>corona-imagesheet</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>lua</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Project/shooting/sprites/default{v}.lua</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <true/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">default/Aliens/Green/hurt.png</key>
            <key type="filename">default/Aliens/Green/stand.png</key>
            <key type="filename">default/Astronauts/1.png</key>
            <key type="filename">default/Astronauts/10.png</key>
            <key type="filename">default/Astronauts/11.png</key>
            <key type="filename">default/Astronauts/12.png</key>
            <key type="filename">default/Astronauts/13.png</key>
            <key type="filename">default/Astronauts/14.png</key>
            <key type="filename">default/Astronauts/15 Copy.png</key>
            <key type="filename">default/Astronauts/15.png</key>
            <key type="filename">default/Astronauts/16.png</key>
            <key type="filename">default/Astronauts/17.png</key>
            <key type="filename">default/Astronauts/18.png</key>
            <key type="filename">default/Astronauts/2.png</key>
            <key type="filename">default/Astronauts/3.png</key>
            <key type="filename">default/Astronauts/4.png</key>
            <key type="filename">default/Astronauts/5.png</key>
            <key type="filename">default/Astronauts/6.png</key>
            <key type="filename">default/Astronauts/7.png</key>
            <key type="filename">default/Astronauts/8.png</key>
            <key type="filename">default/Astronauts/9.png</key>
            <key type="filename">default/Buildings/1.png</key>
            <key type="filename">default/Buildings/10.png</key>
            <key type="filename">default/Buildings/11.png</key>
            <key type="filename">default/Buildings/12.png</key>
            <key type="filename">default/Buildings/13 Copy.png</key>
            <key type="filename">default/Buildings/13.png</key>
            <key type="filename">default/Buildings/2.png</key>
            <key type="filename">default/Buildings/3.png</key>
            <key type="filename">default/Buildings/4.png</key>
            <key type="filename">default/Buildings/5.png</key>
            <key type="filename">default/Buildings/6.png</key>
            <key type="filename">default/Buildings/7.png</key>
            <key type="filename">default/Buildings/8.png</key>
            <key type="filename">default/Buildings/9.png</key>
            <key type="filename">default/Missiles/1.png</key>
            <key type="filename">default/Missiles/10.png</key>
            <key type="filename">default/Missiles/11.png</key>
            <key type="filename">default/Missiles/12.png</key>
            <key type="filename">default/Missiles/13.png</key>
            <key type="filename">default/Missiles/14.png</key>
            <key type="filename">default/Missiles/15.png</key>
            <key type="filename">default/Missiles/16.png</key>
            <key type="filename">default/Missiles/17.png</key>
            <key type="filename">default/Missiles/18.png</key>
            <key type="filename">default/Missiles/19.png</key>
            <key type="filename">default/Missiles/2.png</key>
            <key type="filename">default/Missiles/20.png</key>
            <key type="filename">default/Missiles/21.png</key>
            <key type="filename">default/Missiles/22.png</key>
            <key type="filename">default/Missiles/23.png</key>
            <key type="filename">default/Missiles/24.png</key>
            <key type="filename">default/Missiles/25.png</key>
            <key type="filename">default/Missiles/26.png</key>
            <key type="filename">default/Missiles/27.png</key>
            <key type="filename">default/Missiles/28.png</key>
            <key type="filename">default/Missiles/29.png</key>
            <key type="filename">default/Missiles/3.png</key>
            <key type="filename">default/Missiles/30.png</key>
            <key type="filename">default/Missiles/31.png</key>
            <key type="filename">default/Missiles/32.png</key>
            <key type="filename">default/Missiles/33.png</key>
            <key type="filename">default/Missiles/34.png</key>
            <key type="filename">default/Missiles/35.png</key>
            <key type="filename">default/Missiles/36.png</key>
            <key type="filename">default/Missiles/37.png</key>
            <key type="filename">default/Missiles/38.png</key>
            <key type="filename">default/Missiles/39.png</key>
            <key type="filename">default/Missiles/4.png</key>
            <key type="filename">default/Missiles/40.png</key>
            <key type="filename">default/Missiles/5.png</key>
            <key type="filename">default/Missiles/6.png</key>
            <key type="filename">default/Missiles/7.png</key>
            <key type="filename">default/Missiles/8.png</key>
            <key type="filename">default/Missiles/9.png</key>
            <key type="filename">default/Power-ups/Bolt/1.png</key>
            <key type="filename">default/Power-ups/Bolt/2.png</key>
            <key type="filename">default/Power-ups/Bolt/3.png</key>
            <key type="filename">default/Power-ups/Bolt/4.png</key>
            <key type="filename">default/Power-ups/Bolt/5.png</key>
            <key type="filename">default/Power-ups/Bolt/6.png</key>
            <key type="filename">default/Power-ups/Bolt/7.png</key>
            <key type="filename">default/Power-ups/None/1.png</key>
            <key type="filename">default/Power-ups/None/2.png</key>
            <key type="filename">default/Power-ups/None/3.png</key>
            <key type="filename">default/Power-ups/None/4.png</key>
            <key type="filename">default/Power-ups/Pills/1.png</key>
            <key type="filename">default/Power-ups/Pills/2.png</key>
            <key type="filename">default/Power-ups/Pills/3.png</key>
            <key type="filename">default/Power-ups/Pills/4.png</key>
            <key type="filename">default/Power-ups/Shield/1.png</key>
            <key type="filename">default/Power-ups/Shield/2.png</key>
            <key type="filename">default/Power-ups/Shield/3.png</key>
            <key type="filename">default/Power-ups/Shield/4.png</key>
            <key type="filename">default/Power-ups/Shield/5.png</key>
            <key type="filename">default/Power-ups/Shield/6.png</key>
            <key type="filename">default/Power-ups/Shield/7.png</key>
            <key type="filename">default/Power-ups/Star/1.png</key>
            <key type="filename">default/Power-ups/Star/2.png</key>
            <key type="filename">default/Power-ups/Star/3.png</key>
            <key type="filename">default/Power-ups/Star/4.png</key>
            <key type="filename">default/Power-ups/Star/5.png</key>
            <key type="filename">default/Power-ups/Star/6.png</key>
            <key type="filename">default/Power-ups/Star/7.png</key>
            <key type="filename">default/Power-ups/Things/1.png</key>
            <key type="filename">default/Power-ups/Things/2.png</key>
            <key type="filename">default/Power-ups/Things/3.png</key>
            <key type="filename">default/Ruby/1.png</key>
            <key type="filename">default/Ruby/2.png</key>
            <key type="filename">default/Ruby/3.png</key>
            <key type="filename">default/Ruby/4.png</key>
            <key type="filename">default/Ships/Parts/Beams/1.png</key>
            <key type="filename">default/Ships/Parts/Beams/10.png</key>
            <key type="filename">default/Ships/Parts/Beams/11.png</key>
            <key type="filename">default/Ships/Parts/Beams/12.png</key>
            <key type="filename">default/Ships/Parts/Beams/13.png</key>
            <key type="filename">default/Ships/Parts/Beams/14.png</key>
            <key type="filename">default/Ships/Parts/Beams/2.png</key>
            <key type="filename">default/Ships/Parts/Beams/3.png</key>
            <key type="filename">default/Ships/Parts/Beams/4.png</key>
            <key type="filename">default/Ships/Parts/Beams/5.png</key>
            <key type="filename">default/Ships/Parts/Beams/6.png</key>
            <key type="filename">default/Ships/Parts/Beams/7.png</key>
            <key type="filename">default/Ships/Parts/Beams/8.png</key>
            <key type="filename">default/Ships/Parts/Beams/9.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/1.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/10.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/11.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/12.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/13.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/14.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/15.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/16.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/17.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/18.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/19.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/2.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/20.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/21.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/22.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/23.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/24.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/25.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/26.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/27.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/28.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/29.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/3.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/30.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/31.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/32.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/4.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/5.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/6.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/7.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/8.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/9.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Bases/1.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Bases/10.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Bases/11.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Bases/12.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Bases/13.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Bases/14.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Bases/15.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Bases/16.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Bases/17.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Bases/18.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Bases/2.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Bases/3.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Bases/4.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Bases/5.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Bases/6.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Bases/7.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Bases/8.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Bases/9.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Glass/1.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Glass/10.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Glass/11.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Glass/12.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Glass/13.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Glass/14.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Glass/15.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Glass/16.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Glass/17.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Glass/18.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Glass/19.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Glass/2.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Glass/20.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Glass/21.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Glass/22.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Glass/23.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Glass/24.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Glass/25.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Glass/26.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Glass/3.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Glass/4.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Glass/5.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Glass/6.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Glass/7.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Glass/8.png</key>
            <key type="filename">default/Ships/Parts/Cockpits/Glass/9.png</key>
            <key type="filename">default/Ships/Parts/Engines/1.png</key>
            <key type="filename">default/Ships/Parts/Engines/2.png</key>
            <key type="filename">default/Ships/Parts/Engines/3.png</key>
            <key type="filename">default/Ships/Parts/Engines/4.png</key>
            <key type="filename">default/Ships/Parts/Engines/5.png</key>
            <key type="filename">default/Ships/Parts/Engines/6.png</key>
            <key type="filename">default/Ships/Parts/Engines/7.png</key>
            <key type="filename">default/Ships/Parts/Engines/8.png</key>
            <key type="filename">default/Ships/Parts/Engines/9.png</key>
            <key type="filename">default/Ships/Parts/Guns/1.png</key>
            <key type="filename">default/Ships/Parts/Guns/10.png</key>
            <key type="filename">default/Ships/Parts/Guns/11.png</key>
            <key type="filename">default/Ships/Parts/Guns/12.png</key>
            <key type="filename">default/Ships/Parts/Guns/13.png</key>
            <key type="filename">default/Ships/Parts/Guns/14.png</key>
            <key type="filename">default/Ships/Parts/Guns/15.png</key>
            <key type="filename">default/Ships/Parts/Guns/2.png</key>
            <key type="filename">default/Ships/Parts/Guns/3.png</key>
            <key type="filename">default/Ships/Parts/Guns/4.png</key>
            <key type="filename">default/Ships/Parts/Guns/5.png</key>
            <key type="filename">default/Ships/Parts/Guns/6.png</key>
            <key type="filename">default/Ships/Parts/Guns/7.png</key>
            <key type="filename">default/Ships/Parts/Guns/8.png</key>
            <key type="filename">default/Ships/Parts/Guns/9.png</key>
            <key type="filename">default/Ships/Parts/Others/1.png</key>
            <key type="filename">default/Ships/Parts/Others/2.png</key>
            <key type="filename">default/Ships/Parts/Others/3.png</key>
            <key type="filename">default/Ships/Parts/Others/4.png</key>
            <key type="filename">default/Ships/Parts/Others/5.png</key>
            <key type="filename">default/Ships/Parts/Scratches/1.png</key>
            <key type="filename">default/Ships/Parts/Scratches/2.png</key>
            <key type="filename">default/Ships/Parts/Wings/1.png</key>
            <key type="filename">default/Ships/Parts/Wings/10.png</key>
            <key type="filename">default/Ships/Parts/Wings/11.png</key>
            <key type="filename">default/Ships/Parts/Wings/12.png</key>
            <key type="filename">default/Ships/Parts/Wings/13.png</key>
            <key type="filename">default/Ships/Parts/Wings/14.png</key>
            <key type="filename">default/Ships/Parts/Wings/15.png</key>
            <key type="filename">default/Ships/Parts/Wings/16.png</key>
            <key type="filename">default/Ships/Parts/Wings/17.png</key>
            <key type="filename">default/Ships/Parts/Wings/18.png</key>
            <key type="filename">default/Ships/Parts/Wings/19.png</key>
            <key type="filename">default/Ships/Parts/Wings/2.png</key>
            <key type="filename">default/Ships/Parts/Wings/20.png</key>
            <key type="filename">default/Ships/Parts/Wings/21.png</key>
            <key type="filename">default/Ships/Parts/Wings/22.png</key>
            <key type="filename">default/Ships/Parts/Wings/23.png</key>
            <key type="filename">default/Ships/Parts/Wings/24.png</key>
            <key type="filename">default/Ships/Parts/Wings/25.png</key>
            <key type="filename">default/Ships/Parts/Wings/26.png</key>
            <key type="filename">default/Ships/Parts/Wings/27.png</key>
            <key type="filename">default/Ships/Parts/Wings/28.png</key>
            <key type="filename">default/Ships/Parts/Wings/29.png</key>
            <key type="filename">default/Ships/Parts/Wings/3.png</key>
            <key type="filename">default/Ships/Parts/Wings/30.png</key>
            <key type="filename">default/Ships/Parts/Wings/31.png</key>
            <key type="filename">default/Ships/Parts/Wings/32.png</key>
            <key type="filename">default/Ships/Parts/Wings/33.png</key>
            <key type="filename">default/Ships/Parts/Wings/34.png</key>
            <key type="filename">default/Ships/Parts/Wings/35.png</key>
            <key type="filename">default/Ships/Parts/Wings/36.png</key>
            <key type="filename">default/Ships/Parts/Wings/37.png</key>
            <key type="filename">default/Ships/Parts/Wings/38.png</key>
            <key type="filename">default/Ships/Parts/Wings/39.png</key>
            <key type="filename">default/Ships/Parts/Wings/4.png</key>
            <key type="filename">default/Ships/Parts/Wings/40.png</key>
            <key type="filename">default/Ships/Parts/Wings/41.png</key>
            <key type="filename">default/Ships/Parts/Wings/42.png</key>
            <key type="filename">default/Ships/Parts/Wings/43.png</key>
            <key type="filename">default/Ships/Parts/Wings/44.png</key>
            <key type="filename">default/Ships/Parts/Wings/45.png</key>
            <key type="filename">default/Ships/Parts/Wings/46.png</key>
            <key type="filename">default/Ships/Parts/Wings/47.png</key>
            <key type="filename">default/Ships/Parts/Wings/48.png</key>
            <key type="filename">default/Ships/Parts/Wings/49.png</key>
            <key type="filename">default/Ships/Parts/Wings/5.png</key>
            <key type="filename">default/Ships/Parts/Wings/50.png</key>
            <key type="filename">default/Ships/Parts/Wings/51.png</key>
            <key type="filename">default/Ships/Parts/Wings/52.png</key>
            <key type="filename">default/Ships/Parts/Wings/53.png</key>
            <key type="filename">default/Ships/Parts/Wings/54.png</key>
            <key type="filename">default/Ships/Parts/Wings/55.png</key>
            <key type="filename">default/Ships/Parts/Wings/56.png</key>
            <key type="filename">default/Ships/Parts/Wings/57.png</key>
            <key type="filename">default/Ships/Parts/Wings/58.png</key>
            <key type="filename">default/Ships/Parts/Wings/59.png</key>
            <key type="filename">default/Ships/Parts/Wings/6.png</key>
            <key type="filename">default/Ships/Parts/Wings/60.png</key>
            <key type="filename">default/Ships/Parts/Wings/61.png</key>
            <key type="filename">default/Ships/Parts/Wings/62.png</key>
            <key type="filename">default/Ships/Parts/Wings/63.png</key>
            <key type="filename">default/Ships/Parts/Wings/64.png</key>
            <key type="filename">default/Ships/Parts/Wings/65.png</key>
            <key type="filename">default/Ships/Parts/Wings/66.png</key>
            <key type="filename">default/Ships/Parts/Wings/67.png</key>
            <key type="filename">default/Ships/Parts/Wings/68.png</key>
            <key type="filename">default/Ships/Parts/Wings/7.png</key>
            <key type="filename">default/Ships/Parts/Wings/8.png</key>
            <key type="filename">default/Ships/Parts/Wings/9.png</key>
            <key type="filename">default/Turrets/1.png</key>
            <key type="filename">default/Turrets/2.png</key>
            <key type="filename">default/Turrets/3.png</key>
            <key type="filename">default/Turrets/Parts/Bases/1.png</key>
            <key type="filename">default/Turrets/Parts/Bases/2.png</key>
            <key type="filename">default/Turrets/Parts/Bases/3.png</key>
            <key type="filename">default/Turrets/Parts/Bases/4.png</key>
            <key type="filename">default/Turrets/Parts/Cannos/1.png</key>
            <key type="filename">default/Turrets/Parts/Cannos/2.png</key>
            <key type="filename">default/Turrets/Parts/Cannos/3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>default</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
    </struct>
</data>
